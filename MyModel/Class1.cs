﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyModel
{
    [Serializable]
    public class Class1
    {
        public int A { get; set; }

        public string B { get; set; }

        public DateTime C { get; set; }

        public decimal? D { get; set; }

        public Guid E { get; set; }

        public DateTime? F { get; set; }
    }
}
