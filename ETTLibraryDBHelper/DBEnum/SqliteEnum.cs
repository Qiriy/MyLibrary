﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ETT.Library.DBHelper.DBEnum
{
    /// <summary>
    /// SQLite处理枚举
    /// </summary>
    public enum SqliteEnum
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("新增")]
        Add =0,

        /// <summary>
        /// 
        /// </summary>
        [Description("查询")]
        Search = 1,

        /// <summary>
        /// 
        /// </summary>
        [Description("删除")]
        Remove = 2,

        /// <summary>
        /// 
        /// </summary>
        [Description("修改")]
        Update = 3
    }
}
