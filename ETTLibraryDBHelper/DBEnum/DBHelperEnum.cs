﻿namespace ETT.Library.DBHelper.DBEnum
{
    /// <summary>
    /// DBHelper类枚举
    /// </summary>
    public enum DBHelperTranEnum
    {
        /// <summary>
        /// 打开链接
        /// </summary>
        OPEN = 0,
        /// <summary>
        /// 继续执行
        /// </summary>
        CONTINUE = 1,
        /// <summary>
        /// 提交更改
        /// </summary>
        SUBMIT = 2,
        /// <summary>
        /// 回滚操作
        /// </summary>
        ROLLBACK = 3
    }
}
