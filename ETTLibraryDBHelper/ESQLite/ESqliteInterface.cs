﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ETT.Library.DBHelper.DBEnum;

namespace ETT.Library.DBHelper.ESQLite
{
    /// <summary>
    /// SQLite 对外接口
    /// </summary>
    public class ESqliteInterface
    {
        /// <summary>
        /// TODO 多线程锁 
        /// </summary>
        private static readonly object Objectlock = new object();

        /// <summary>
        /// SQLite对外执行接口
        /// </summary>
        /// <param name="comdtext"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static dynamic SqliteExec(string comdtext, SqliteEnum type)
        {
            lock (Objectlock)
            {
                dynamic refvalue;
                var sqliteconn = SqliteConnection.GetInstance();
                switch (type)
                {
                    case SqliteEnum.Add:
                    case SqliteEnum.Remove:
                    case SqliteEnum.Update:
                        refvalue = sqliteconn.ExecuteNonQuery(comdtext);
                        break;
                    case SqliteEnum.Search:
                        refvalue = sqliteconn.GetDataTable(comdtext);
                        break;
                    default:
                        refvalue = sqliteconn.ExecuteScalar(comdtext);
                        break;
                }
                Thread.Sleep(100);
                return refvalue;
            }
        }

        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnStr {
            get { return SqliteConnectionString.ConnectionStr; }
        }
    }
}
