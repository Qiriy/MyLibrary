﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace ETT.Library.DBHelper.ESQLite
{
    /// <summary>
    /// SQLite 帮助类
    /// </summary>
    internal class SqliteConnection
    {
        
        private static SqliteConnection _instance;
        private static readonly object Locker = new object ();

        /// <summary>
        /// SQLite构造函数
        /// </summary>
        private SqliteConnection()
        {
        }

        /// <summary>
        /// 获取实例
        /// </summary>
        /// <returns></returns>
        public static SqliteConnection GetInstance()
        {
            if (_instance == null )
            {
                lock (Locker)
                {
                    if (_instance == null )
                    {
                        _instance = new SqliteConnection();
                    }
                }
            }

            return _instance;
        }

        /// <summary>
        /// 获取数据表
        /// </summary>
        /// <param name="cmdText"> 需要执行的命令文本 </param>
        /// <returns> 一个数据表集合 </returns>
        public DataTable GetDataTable( string cmdText)
        {
            var dt = new DataTable();

            try
            {
                using (var conn = new SQLiteConnection(SqliteConnectionString.ConnectionStr))
                {
                    conn.Open();
                    var cmd = new SQLiteCommand(conn) {CommandText = cmdText};
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch (Exception e)
            {
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }

            return dt;
        }

        /// <summary>
        /// 执行非查询命令
        /// </summary>
        /// <param name="cmdText"> 需要执行的命令文本 </param>
        /// <returns> 返回更新的行数 </returns>
        public int ExecuteNonQuery( string cmdText)
        {
            using (var conn = new SQLiteConnection(SqliteConnectionString.ConnectionStr))
            {
                conn.Open();
                var cmd = new SQLiteCommand(conn) {CommandText = cmdText};
                var rowsUpdated = cmd.ExecuteNonQuery();

                return rowsUpdated;
            }
        }

        /// <summary>
        /// 执行检索单项命令
        /// </summary>
        /// <param name="cmdText"> 需要执行的命令文本 </param>
        /// <returns> 一个字符串 </returns>
        public string ExecuteScalar( string cmdText)
        {
            using (var conn = new SQLiteConnection(SqliteConnectionString.ConnectionStr))
            {
                conn.Open();
                var cmd = new SQLiteCommand(conn) {CommandText = cmdText};
                var value = cmd.ExecuteScalar();

                if (value != null )
                {
                    return value.ToString();
                }
            }
            return "" ;
        }
    }
}
