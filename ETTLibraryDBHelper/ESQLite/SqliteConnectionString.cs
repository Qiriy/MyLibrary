﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ETT.Library.DBHelper.ESQLite
{
    /// <summary>
    /// SQLite数据库连接字符串
    /// </summary>
    public static class SqliteConnectionString
    {
        /// <summary>
        /// 连接字符串AppsettingKey名
        /// </summary>
        private static readonly string AppSettingConStr = ConfigurationManager.AppSettings["SQLiteName"];

        /// <summary>
        /// 数据库连接字符串
        /// </summary>
        internal static readonly string ConnectionStr = ConfigurationManager.AppSettings[AppSettingConStr];

    }
}
