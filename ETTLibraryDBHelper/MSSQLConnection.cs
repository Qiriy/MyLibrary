﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.DBHelper
{
    /// <summary>
    ///SQLConnection类
    /// </summary>
    public class MssqlConnection
    {
        /// <summary>
        /// 获取SQLConnection
        /// </summary>
        /// <param name="strname"></param>
        /// <returns></returns>
        public static SqlConnection GetConnection(string strname = null)
        {
            SqlConnection conn;
            try
            {
                conn = !string.IsNullOrWhiteSpace(ConnectionString.CONNECTIONSTRING) ? new SqlConnection(ConnectionString.CONNECTIONSTRING) : new SqlConnection(ConfigurationManager.AppSettings[strname]);
            }
            catch (Exception)
            {
                throw new Exception(@"数据库连接字符串配置错误，请检查数据库链接字符串设置！");
            }
            return conn;
        }
    }
}
