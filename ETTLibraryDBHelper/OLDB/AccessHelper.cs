﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace ETT.Library.DBHelper.OLDB
{
    /// <summary>
    /// Access帮助类
    /// </summary>
    public abstract class AccessHelper
    {

        #region 变量
        /// <summary>
        /// OLEDB链接类
        /// </summary>
        protected static OleDbConnection Conn = new OleDbConnection();
        /// <summary>
        /// OLEDBCommand类
        /// </summary>
        protected static OleDbCommand Comm = new OleDbCommand();
        /// <summary>
        /// 数据库链接字符串
        /// </summary>
        protected static string CONNECTIONSTRING = System.Configuration.ConfigurationManager.AppSettings["AccessDBString"];

        #endregion

        #region 构造函数

        /// <summary>
        /// 构造函数
        /// </summary>
        public AccessHelper()
        {

        }

        #endregion

        #region 打开数据库

        /// <summary>
        /// 打开数据库
        /// </summary>
        private static void OpenConnection()
        {
            if (Conn.State == ConnectionState.Closed)
            {
                Conn.ConnectionString = CONNECTIONSTRING;
                Comm.Connection = Conn;
                try
                {
                    Conn.Open();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        #endregion

        #region 关闭数据库

        /// <summary>
        /// 关闭数据库
        /// </summary>
        private static void CloseConnection()
        {
            if (Conn.State == ConnectionState.Open)
            {
                Conn.Close();
                Conn.Dispose();
                Comm.Dispose();
            }
        }

        #endregion

        #region 执行sql语句

        /// <summary>
        /// 执行sql语句
        /// </summary>
        public static void ExecuteSql(string sqlstr)
        {
            try
            {
                OpenConnection();
                Comm.CommandType = CommandType.Text;
                Comm.CommandText = sqlstr;
                Comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region 返回指定sql语句的OleDbDataReader对象,使用时请注意关闭这个对象。

        /// <summary>
        /// 返回指定sql语句的OleDbDataReader对象,使用时请注意关闭这个对象。
        /// </summary>
        public static OleDbDataReader DataReader(string sqlstr)
        {
            OleDbDataReader dr = null;
            try
            {
                OpenConnection();
                Comm.CommandText = sqlstr;
                Comm.CommandType = CommandType.Text;

                dr = Comm.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch
            {
                try
                {
                    dr.Close();
                    CloseConnection();
                }
                catch (Exception e)
                {
                    ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
                }
            }
            return dr;
        }

        #endregion

        #region 返回指定sql语句的OleDbDataReader对象,使用时请注意关闭

        /// <summary>
        /// 返回指定sql语句的OleDbDataReader对象,使用时请注意关闭
        /// </summary>
        public static void DataReader(string sqlstr, ref OleDbDataReader dr)
        {
            try
            {
                OpenConnection();
                Comm.CommandText = sqlstr;
                Comm.CommandType = CommandType.Text;
                dr = Comm.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch
            {
                try
                {
                    if (dr != null && !dr.IsClosed)
                        dr.Close();
                }
                catch (Exception e)
                {
                    ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
                }
                finally
                {
                    CloseConnection();
                }
            }
        }

        #endregion

        #region 返回指定sql语句的DataSet

        /// <summary>
        /// 返回指定sql语句的DataSet
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <returns></returns>
        public static DataSet DataSet(string sqlstr)
        {
            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter();
            try
            {
                OpenConnection();
                Comm.CommandType = CommandType.Text;
                Comm.CommandText = sqlstr;
                da.SelectCommand = Comm;
                da.Fill(ds);

            }
            catch (Exception e)
            {
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            finally
            {
                CloseConnection();
            }
            return ds;
        }

        #endregion

        #region 返回指定sql语句的DataSet

        /// <summary>
        /// 返回指定sql语句的DataSet
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <param name="ds"></param>
        public static void DataSet(string sqlstr, ref DataSet ds)
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            try
            {
                OpenConnection();
                Comm.CommandType = CommandType.Text;
                Comm.CommandText = sqlstr;
                da.SelectCommand = Comm;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region 返回指定sql语句的DataTable

        /// <summary>
        /// 返回指定sql语句的DataTable
        /// </summary>
        public static void DataTable(string sqlstr, ref DataTable dt)
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            try
            {
                OpenConnection();
                Comm.CommandType = CommandType.Text;
                Comm.CommandText = sqlstr;
                da.SelectCommand = Comm;
                da.Fill(dt);
            }
            catch (Exception e)
            {
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region 返回指定sql语句的DataView

        /// <summary>
        /// 返回指定sql语句的DataView
        /// </summary>
        /// <param name="sqlstr"></param>
        /// <returns></returns>
        public static DataView DataView(string sqlstr)
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataView dv = new DataView();
            DataSet ds = new DataSet();
            try
            {
                OpenConnection();
                Comm.CommandType = CommandType.Text;
                Comm.CommandText = sqlstr;
                da.SelectCommand = Comm;
                da.Fill(ds);
                dv = ds.Tables[0].DefaultView;
            }
            catch (Exception e)
            {
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            finally
            {
                CloseConnection();
            }
            return dv;
        }

        #endregion
    }
}