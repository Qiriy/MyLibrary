﻿using System;
using System.Data;
using System.Data.SqlClient;
using ETT.Library.ETTHelper.LogHelper;

namespace ETT.Library.DBHelper
{
    /// <summary>
    /// 数据连接和事务类
    /// </summary>
    public class MSSQLTran : IDisposable
    {
        #region<<构造函数>>

        /// <summary>
        /// MSSQLTran 构造函数
        /// </summary>
        public MSSQLTran()
        {
            ConnSqlConnection();
        }
        /// <summary>
        /// MSSQLTran 构造函数
        /// <param name="connstring">连接字符串</param>
        /// </summary>
        public MSSQLTran(string connstring)
        {
            ConnSqlConnection(connstring);
        }
        #endregion

        #region<<析构函数>>

        /// <summary>
        /// 析构 Microsoft SQL Server 事物类
        /// </summary>
        ~MSSQLTran()
        {
            if (_sqlConn.State == System.Data.ConnectionState.Open)
            {
                _sqlConn.Dispose();
            }
        }

        #endregion

        #region<<属性>>

        private SqlConnection _sqlConn;
        private SqlTransaction _sqlTran;

        /// <summary>
        /// SQL数据库连接
        /// </summary>
        public SqlConnection SqlConn
        {
            get { return _sqlConn; }
            set { _sqlConn = value; }
        }

        /// <summary>
        /// SQL数据库连接事务
        /// </summary>
        public SqlTransaction SqlTran
        {
            get { return _sqlTran; }
        }

        /// <summary>
        /// <para>内容：[true] 为已经开启，[false]为未开启</para>
        /// <para>描述：数据库连接是否开启</para>
        /// </summary>
        public bool IsOpenConn
        {
            get
            {
                bool bResult = _sqlConn.State == ConnectionState.Open;
                return bResult;
            }
        }

        #endregion

        #region<<公有方法>>

        /// <summary>
        /// 释放MSSQLTran
        /// </summary>
        public void Dispose()
        {
            if (_sqlConn.State == System.Data.ConnectionState.Open)
            {
                _sqlTran = null;
                _sqlConn.Dispose();
            }
        }

        /// <summary>
        /// 开始事务
        /// </summary>
        public void BeginTran()
        {
            if (_sqlConn != null)
            {
                _sqlTran = SqlConn.BeginTransaction();
            }
        }

        /// <summary>
        /// 回滚事务
        /// </summary>
        public void RollBackTran()
        {
            if (_sqlTran != null)
            {
                _sqlTran.Rollback();
            }
        }

        /// <summary>
        /// 提交事务
        /// </summary>
        public void SubmitTran()
        {
            if (_sqlTran != null)
            {
                _sqlTran.Commit();
            }
        }

        /// <summary>
        /// 打开SQL事务
        /// </summary>
        public void OpenConn()
        {
            if (_sqlConn == null)
            {
                ConnSqlConnection();
            }
            if (_sqlConn != null && _sqlConn.State != System.Data.ConnectionState.Open)
            {
                _sqlConn.Open();
            }
        }

        #endregion

        #region<<私有方法>>

        private void ConnSqlConnection(string connstring = "")
        {
            try
            {
                var conn = string.IsNullOrWhiteSpace(connstring) ? ConnectionString.CONNECTIONSTRING : connstring;
                if (!string.IsNullOrWhiteSpace(conn))
                {
                    _sqlConn = new SqlConnection(conn);
                    _sqlConn.Open();
                }
                else
                {
                    WriteError.WriteErrorLog(CONNECTIONSTRINGISNULL);
                }
            }
            catch (Exception e)
            {
                WriteError.WriteErrorLog(e.Message);
            }
        }

        #endregion

        #region<<常量>>

        /// <summary>
        /// <para>内容：未获取到数据库连接字符串</para>
        /// <para>描述：数据库连接字符串未获取到时弹出的提示</para>
        /// </summary>
        private const string CONNECTIONSTRINGISNULL = @"未获取到数据库连接字符串！";

        #endregion

    }
}
