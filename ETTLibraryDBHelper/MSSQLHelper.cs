﻿using System;
using System.Data;
using System.Data.SqlClient;
using ETT.Library.ETTHelper;
using ETT.Library.ETTHelper.LogHelper;

namespace ETT.Library.DBHelper
{
    /// <summary>
    /// Microsoft SQL Server数据库连接帮助类
    /// </summary>
    public class MSSQLHelper
    {
        #region<<传入参数不带数据库连接字符串>>

        #region<<公有方法>>


        #region<<不带参数的SQL语句>>

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="strSql">T-SQL语句</param>
        /// <returns></returns>
        public static bool Exists(string strSql)
        {
            object obj = GetSingle(strSql);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = SafeConvert.ToInt32(obj.ToString()); //也可能=0
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="strSql">T-SQL语句</param>
        /// <param name="sqlcon"></param>
        /// <param name="sqltran"></param>
        /// <param name="cmdParms"></param>
        /// <returns></returns>
        public static bool Exists(string strSql, SqlConnection sqlcon, SqlTransaction sqltran, params
            SqlParameter[] cmdParms)
        {
            object obj = GetSingle(strSql, sqlcon, sqltran, cmdParms);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = SafeConvert.ToInt32(obj.ToString()); //也可能=0
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="strSql">T-SQL语句</param>
        /// <param name="cmdParms">SqlParameter参数数组</param>
        /// <returns></returns>
        public static bool Exists(string strSql, params SqlParameter[] cmdParms)
        {
            object obj = GetSingle(strSql, cmdParms);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = SafeConvert.ToInt32(obj.ToString());
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="sqlString">T-SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string sqlString)
        {
            int rows = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING))
            {
                using (SqlCommand cmd = new SqlCommand(sqlString, connection))
                {
                    try
                    {
                        connection.Open();
                        rows = cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return rows;
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="sqlString">计算查询结果T-SQL语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string sqlString)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING))
            {
                using (SqlCommand cmd = new SqlCommand(sqlString, connection))
                {
                    try
                    {
                        connection.Open();
                        object obj = cmd.ExecuteScalar();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="strSql">查询T-SQL语句</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string strSql)
        {
            SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING);
            SqlDataReader myReader = null;
            SqlCommand cmd = new SqlCommand(strSql, connection);
            try
            {
                connection.Open();
                myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                throw e;
            }
            return myReader;
        }


        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string sqlString)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter command = new SqlDataAdapter(sqlString, connection);
                    command.Fill(ds, "ds");
                }
                catch (Exception e)
                {
                    throw e;
                }
                return ds;
            }
        }

        #endregion

        #region<<带参数的SQL语句>>

        /// <summary>
        /// 执行查询语句，返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <param name="cmdParms">sql参数</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string sqlString, params SqlParameter[] cmdParms)
        {
            SqlDataReader myReader = null;
            using (SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING))
            {

                SqlCommand cmd = new SqlCommand();
                try
                {
                    PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                    myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    cmd.Parameters.Clear();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return myReader;
        }

        /// <summary>
        /// 执行带有存储过程的查询语句，返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <param name="sqlcon">数据库连接</param>
        /// <param name="sqltran">sql事务</param>
        /// <param name="cmdParms">sql参数</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string sqlString, SqlConnection sqlcon, SqlTransaction sqltran, params
            SqlParameter[] cmdParms)
        {
            SqlDataReader myReader = null;
            SqlCommand cmd = new SqlCommand();
            try
            {
                PrepareCommand(cmd, sqlcon, sqltran, sqlString, cmdParms);
                myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
            }
            catch (Exception e)
            {
                throw e;
            }
            return myReader;
        }


        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <param name="cmdParms">sql参数</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string sqlString, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING))
            {
                SqlCommand cmd = new SqlCommand();
                if (!string.IsNullOrWhiteSpace(ConnectionString.TIMTOUTLONG)) {
                    var time = SafeConvert.ToInt32(ConnectionString.TIMTOUTLONG);
                    cmd.CommandTimeout = time == 0 ? 180 : time;
                }
                PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, "ds");
                        cmd.Parameters.Clear();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    return ds;
                }
            }
        }

        /// <summary>
        /// 执行带事务查询语句，返回DataSet
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <param name="sqlcon">数据库连接</param>
        /// <param name="sqltran">sql事务</param>
        /// <param name="cmdParms">sql参数</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string sqlString, SqlConnection sqlcon, SqlTransaction sqltran, params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrWhiteSpace(ConnectionString.TIMTOUTLONG))
            {
                var time = SafeConvert.ToInt32(ConnectionString.TIMTOUTLONG);
                cmd.CommandTimeout = time == 0 ? 180 : time;
            }
            PrepareCommand(cmd, sqlcon, sqltran, sqlString, cmdParms);
            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                DataSet ds = new DataSet();
                try
                {
                    da.Fill(ds, "ds");
                    cmd.Parameters.Clear();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return ds;
            }
        }


        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="sqlString">计算查询结果T-SQL语句</param>
        /// <param name="cmdParms">sql参数</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string sqlString, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    if (!string.IsNullOrWhiteSpace(ConnectionString.TIMTOUTLONG))
                    {
                        var time = SafeConvert.ToInt32(ConnectionString.TIMTOUTLONG);
                        cmd.CommandTimeout = time == 0 ? 180 : time;
                    }
                    try
                    {
                        PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                        object obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }

        /// <summary>
        /// 执行一条带事务的 计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="sqlString">计算查询结果T-SQL语句</param>
        /// <param name="sqlcon">数据库连接</param>
        /// <param name="sqltran">sql事务</param>
        /// <param name="cmdParms">sql参数</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string sqlString, SqlConnection sqlcon, SqlTransaction sqltran, params SqlParameter[] cmdParms)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                if (!string.IsNullOrWhiteSpace(ConnectionString.TIMTOUTLONG))
                {
                    var time = SafeConvert.ToInt32(ConnectionString.TIMTOUTLONG);
                    cmd.CommandTimeout = time == 0 ? 180 : time;
                }
                try
                {
                    PrepareCommand(cmd, sqlcon, sqltran, sqlString, cmdParms);
                    object obj = cmd.ExecuteScalar();
                    cmd.Parameters.Clear();
                    if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                    {
                        return null;
                    }
                    else
                    {
                        return obj;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="sqlString">T-SQL语句</param>
        /// <param name="cmdParms">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string sqlString, params SqlParameter[] cmdParms)
        {
            int rows = 0;
            using (SqlConnection connection = new SqlConnection(ConnectionString.CONNECTIONSTRING))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    if (!string.IsNullOrWhiteSpace(ConnectionString.TIMTOUTLONG))
                    {
                        var time = SafeConvert.ToInt32(ConnectionString.TIMTOUTLONG);
                        cmd.CommandTimeout = time == 0 ? 180 : time;
                    }
                    try
                    {
                        PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                        rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            return rows;
        }

        /// <summary>
        /// 执行带事务SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="sqlString">T-SQL语句</param>
        /// <param name="sqlcon">数据库连接</param>
        /// <param name="sqltran">sql事务</param>
        /// <param name="cmdParms">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string sqlString, SqlConnection sqlcon, SqlTransaction sqltran, params SqlParameter[] cmdParms)
        {
            int rows = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                if (!string.IsNullOrWhiteSpace(ConnectionString.TIMTOUTLONG))
                {
                    var time = SafeConvert.ToInt32(ConnectionString.TIMTOUTLONG);
                    cmd.CommandTimeout = time == 0 ? 180 : time;
                }
                try
                {
                    PrepareCommand(cmd, sqlcon, sqltran, sqlString, cmdParms);
                    rows = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return rows;
        }

        #endregion

        #endregion

        #region<<私有方法>>

        /// <summary>
        /// 执行数据库命令
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="conn"></param>
        /// <param name="trans"></param>
        /// <param name="cmdText"></param>
        /// <param name="cmdParms"></param>
        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, params SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
            }
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
            {
                cmd.Transaction = trans;
            }
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (SqlParameter parameter in cmdParms)
                {
                    if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                        (parameter.Value == null))
                    {
                        parameter.Value = DBNull.Value;
                    }
                    cmd.Parameters.Add(parameter);
                }
            }
        }


        #endregion

        #endregion

        #region<<传入参数带数据库连接字符串>>

        #region<<公有方法>>


        #region<<不带参数的SQL语句>>

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="strSql">T-SQL语句</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns></returns>
        public static bool Exists(string strSql, string connstring)
        {
            object obj = GetSingle(strSql, connstring);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = SafeConvert.ToInt32(obj.ToString()); //也可能=0
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="strSql">T-SQL语句</param>
        /// <param name="cmdParms">SqlParameter参数数组</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns></returns>
        public static bool Exists(string strSql, string connstring, params SqlParameter[] cmdParms)
        {
            object obj = GetSingle(strSql, connstring, cmdParms);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = SafeConvert.ToInt32(obj.ToString());
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="sqlString">T-SQL语句</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string sqlString, string connstring)
        {
            int rows = 0;
            using (SqlConnection connection = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand(sqlString, connection))
                {
                    try
                    {
                        connection.Open();
                        rows = cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return rows;
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="sqlString">计算查询结果T-SQL语句</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string sqlString, string connstring)
        {
            using (SqlConnection connection = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand(sqlString, connection))
                {
                    try
                    {
                        connection.Open();
                        object obj = cmd.ExecuteScalar();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="strSql">查询T-SQL语句</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string strSql, string connstring)
        {
            SqlConnection connection = new SqlConnection(connstring);
            SqlDataReader myReader = null;
            SqlCommand cmd = new SqlCommand(strSql, connection);
            try
            {
                connection.Open();
                myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                throw e;
            }
            return myReader;
        }


        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string sqlString, string connstring)
        {
            using (SqlConnection connection = new SqlConnection(connstring))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    SqlDataAdapter command = new SqlDataAdapter(sqlString, connection);
                    command.Fill(ds, "ds");
                }
                catch (Exception e)
                {
                    throw e;
                }
                return ds;
            }
        }

        #endregion

        #region<<带参数的SQL语句>>

        /// <summary>
        /// 执行查询语句，返回SqlDataReader ( 注意：调用该方法后，一定要对SqlDataReader进行Close )
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <param name="cmdParms">sql参数</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string sqlString, string connstring, params SqlParameter[] cmdParms)
        {
            SqlDataReader myReader = null;
            using (SqlConnection connection = new SqlConnection(connstring))
            {

                SqlCommand cmd = new SqlCommand();
                try
                {
                    PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                    myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    cmd.Parameters.Clear();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return myReader;
        }




        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="sqlString">查询T-SQL语句</param>
        /// <param name="cmdParms">sql参数</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string sqlString, string connstring, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(connstring))
            {
                SqlCommand cmd = new SqlCommand();
                PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, "ds");
                        cmd.Parameters.Clear();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    return ds;
                }
            }
        }




        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="sqlString">计算查询结果T-SQL语句</param>
        /// <param name="cmdParms">sql参数</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string sqlString, string connstring, params SqlParameter[] cmdParms)
        {
            using (SqlConnection connection = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                        object obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }



        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="sqlString">T-SQL语句</param>
        /// <param name="cmdParms">SQL语句</param>
        /// <param name="connstring">数据库连接字符串</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string sqlString, string connstring, params SqlParameter[] cmdParms)
        {
            int rows = 0;
            using (SqlConnection connection = new SqlConnection(connstring))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, sqlString, cmdParms);
                        rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
            return rows;
        }
        #endregion

        #endregion

        #endregion

        #region<<执行存储过程>>

        /// <summary>          
        /// ASP.NET数据库驱动类：执行带参的命令式的存储过程          
        /// </summary>         
        /// <param name="procName">存储过程名称</param>          
        /// <param name="paras">为存储过程的参数解决赋参的SqlParameter对象数组          
        /// (每一个SqlParameter对象为一个参数解决赋参)</param>          
        /// <returns>存储过程的返回值</returns>          
        public static int ExecuteProcedure(string procName,
            SqlParameter[] paras)
        {
            SqlConnection sqlConn = null;
            try
            {
                sqlConn = new SqlConnection(ConnectionString.CONNECTIONSTRING);
                sqlConn.Open();
                SqlCommand sqlCmd = new SqlCommand(procName, sqlConn);                  //执行存储过程类型                  
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddRange(paras);
                SqlParameter p = new SqlParameter();                  //取存储过程的返回值                  
                p.Direction = ParameterDirection.ReturnValue;
                p.SqlDbType = SqlDbType.Int;
                sqlCmd.Parameters.Add(p);
                sqlCmd.ExecuteNonQuery();
                int v = p.Value == null ? -1 : Convert.ToInt32(p.Value);
                return v;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlConn != null) sqlConn.Close();
            }
        }
        /// <summary>          
        /// ASP.NET数据库驱动类：执行带参的查询式的存储过程          
        /// </summary>          
        /// <param name="procName">存储过程名称</param>          
        /// <param name="paras">为存储过程的参数解决赋参的SqlParameter对象数组          
        /// (每一个SqlParameter对象为一个参数解决赋参)</param>          
        /// <returns>存储过程执行完毕后如果在数据库服务器端形成一个          
        /// 查询结果集，则返回指向该结果集的一个数据读取器对象</returns>          
        public static SqlDataReader ExecuteProc(string procName, SqlParameter[] paras)
        {
            SqlConnection sqlConn = null;
            try
            {
                sqlConn = new SqlConnection(ConnectionString.CONNECTIONSTRING);
                sqlConn.Open();
                SqlCommand sqlCmd = new SqlCommand(procName, sqlConn);                  //执行存储过程类型                  
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddRange(paras);
                SqlDataReader sqlDr = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
                return sqlDr;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
