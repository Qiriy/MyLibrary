﻿
using System;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using ETT.Library.ETTHelper;

namespace ETT.Library.DBHelper
{
    /// <summary>
    /// 数据库操作时使用的静态变量类
    /// </summary>
    public class ConnectionString
    {

        #region<<字段>>
        /// <summary>
        /// <para>值：默认为空</para>
        /// <para>内容：存放是否加密链接字符串，ture 标识为加密，false或者没有时标识为不加密 </para>
        /// </summary>
        private static string ISEncrypt = System.Configuration.ConfigurationManager.AppSettings["ConnEncrypt"];
        /// <summary>
        /// <para>值：默认为空</para>
        /// <para>内容：存放数据库链接字符串加密密钥应小于等于8位</para>
        /// </summary>
        private static string DESDEY = System.Configuration.ConfigurationManager.AppSettings["DESkey"];

        /// <summary>
        /// <para>值：默认为空</para>
        /// <para>内容：存放数据库连接字符串的静态变量</para>
        /// </summary>
        public static string CONNECTIONSTRING = ISEncrypt == "true" ? ETTHelper.EncryptLibaray.DESEncrypt.Decrypt(System.Configuration.ConfigurationManager.AppSettings["ConnStr1"], DESDEY) 
            : System.Configuration.ConfigurationManager.AppSettings["ConnStr1"];

        /// <summary>
        /// <para>值：默认为空</para>
        /// <para>内容：存放数据库连接字符串超时时间的静态变量,只支持QUERY方法</para>
        /// </summary>
        internal static string TIMTOUTLONG = System.Configuration.ConfigurationManager.AppSettings["SqlTimeOut"];
        #endregion

    }
}
