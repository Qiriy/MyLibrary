﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Caching;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Web;
using System.Web.Caching;

namespace ETT.Library.ETTHelper.CCache
{
    /// <summary>
    /// 内存缓存
    /// </summary>
    public static class CacheHelper
    {
        /// <summary>
        /// 在应用程序的同级目录(主要防止外部访问)
        /// </summary>
        public static readonly string FilePath = ConfigurationManager.AppSettings["FileCache"];


        /// <summary>
        /// 文件扩展名
        /// </summary>
        public static readonly string FileExt = ".cache";

        private static object GetCacheLock = new object();


        /// <summary>
        /// 获取数据缓存
        /// </summary>
        /// <param name="cacheKey">键</param>
        public static object GetCache(string cacheKey)
        {
            lock (GetCacheLock)
            {
                long i = System.Runtime.Caching.MemoryCache.Default.GetCount();
                CacheItem objCache = System.Runtime.Caching.MemoryCache.Default.GetCacheItem(cacheKey);
                if (objCache == null)
                {
                    string filepath = FilePath + cacheKey + FileExt;
                    if (FolderManage.IsFolderExist(FilePath, FileEnum.Create))
                    {
                        if (!File.Exists(filepath))
                        {
                            File.Create(filepath);
                        }
                    }
                    LogWriteLock.EnterReadLock();

                    using (FileStream file = File.OpenRead(filepath))
                    {

                        if (file.CanRead)
                        {
                            //Debug.WriteLine("缓存反序列化获取数据：" + cacheKey);
                            object obj = CacheHelper.BinaryDeSerialize(file);

                            CacheData data = (CacheData) obj;
                            if (data != null)
                            {
                                //判断是否过期
                                if (data.FailureTime >= DateTime.Now)
                                {
                                    //将数据添加到内存
                                    CacheHelper.SetCacheToMemory(cacheKey, data);
                                    LogWriteLock.ExitReadLock();

                                    return data.Value;
                                }
                                else
                                {
                                    //Debug.WriteLine("数据过期：" + cacheKey);

                                    File.Delete(filepath);
                                    LogWriteLock.ExitReadLock();

                                    //数据过期
                                    return null;
                                }
                            }
                            else
                            {
                                LogWriteLock.ExitReadLock();
                                return null;
                            }
                        }
                        else
                        {
                            LogWriteLock.ExitReadLock();
                            return null;
                        }
                    }

                }
                else
                {
                    CacheData data = (CacheData)objCache.Value;
                    return data.Value;
                }

            }

        }
        /// <summary>
        /// 内存缓存数
        /// </summary>
        /// <returns></returns>
        public static object GetCacheCount()
        {
            return System.Runtime.Caching.MemoryCache.Default.GetCount();
        }
        /// <summary>
        /// 文件缓存数
        /// </summary>
        /// <returns></returns>
        public static object GetFileCacheCount()
        {
            DirectoryInfo di = new DirectoryInfo(FilePath);
            return di.GetFiles().Length;
        }

        private static object SetCacheLock = new object();
        static ReaderWriterLockSlim LogWriteLock = new ReaderWriterLockSlim();

        /// <summary>
        /// 设置数据缓存
        /// </summary>
        public static bool SetCache(string cacheKey, object objObject, CacheItemPolicy policy)
        {
            lock (SetCacheLock)
            {
                string _filepath = FilePath + cacheKey + FileExt;
                if (Directory.Exists(FilePath) == false)
                {
                    Directory.CreateDirectory(FilePath);
                }
                //设置缓存数据的相关参数
                CacheData data = new CacheData() { Value = objObject, CreateTime = DateTime.Now, AbsoluteExpiration = policy.AbsoluteExpiration, Priority = policy.Priority };
                CacheItem objCache = new CacheItem(cacheKey, data);
                FileStream stream = null;
                LogWriteLock.EnterWriteLock();
                if (File.Exists(_filepath) == false)
                {
                    stream = new FileStream(_filepath, FileMode.CreateNew, FileAccess.Write, FileShare.Write);
                }
                else
                {
                    stream = new FileStream(_filepath, FileMode.Create, FileAccess.Write, FileShare.Write);
                }
                LogWriteLock.ExitWriteLock();

                // Debug.WriteLine("缓存序列化设置数据：" + cacheKey);
                CacheHelper.BinarySerialize(stream, data);
                stream.Dispose();
                stream.Close();
                return System.Runtime.Caching.MemoryCache.Default.Add(objCache, policy);
            }
        }

        /// <summary>
        /// 将Cache记录到内存
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool SetCacheToMemory(string cacheKey, CacheData data)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            CacheItem objCache = new CacheItem(cacheKey, data);
            policy.AbsoluteExpiration = data.AbsoluteExpiration;
            policy.Priority = System.Runtime.Caching.CacheItemPriority.NotRemovable;
            return System.Runtime.Caching.MemoryCache.Default.Add(objCache, policy);
        }

        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="objObject"></param>
        /// <param name="absoluteExpiration"></param>
        /// <returns></returns>
        public static bool SetCache(string cacheKey, object objObject, DateTimeOffset absoluteExpiration)
        {

            CacheItemPolicy priority = new CacheItemPolicy();
            priority.Priority = System.Runtime.Caching.CacheItemPriority.NotRemovable;
            priority.AbsoluteExpiration = absoluteExpiration;
            return SetCache(cacheKey, objObject, priority);
        }

        /// <summary>
        /// 设置cache
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="objObject"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        public static bool SetCache(string cacheKey, object objObject, System.Runtime.Caching.CacheItemPriority priorityin)
        {

            CacheItemPolicy priority = new CacheItemPolicy();
            priority.Priority = priorityin;
            priority.AbsoluteExpiration = System.Runtime.Caching.ObjectCache.InfiniteAbsoluteExpiration;
            return SetCache(cacheKey, objObject, priority);
        }

        /// <summary>
        /// 设置数据缓存
        /// </summary>
        public static bool SetCache(string cacheKey, object objObject)
        {
            return CacheHelper.SetCache(cacheKey, objObject, System.Runtime.Caching.CacheItemPriority.NotRemovable);
        }

        /// <summary>
        /// 移除指定数据缓存
        /// </summary>
        public static void RemoveCache(string cacheKey)
        {
            System.Runtime.Caching.MemoryCache.Default.Remove(cacheKey);
            string filepath = FilePath + cacheKey + FileExt;
            File.Delete(filepath);
        }

        /// <summary>
        /// 移除全部缓存
        /// </summary>
        public static void RemoveAllCache()
        {
            MemoryCache cache = System.Runtime.Caching.MemoryCache.Default;
            var dictionary = cache.GetValues(null);
            if (dictionary != null)
                foreach (var c in dictionary)
                {
                    cache.Remove(c.Key);
                }
            DirectoryInfo di = new DirectoryInfo(FilePath);
            di.Delete(true);
        }
        /// <summary>
        /// 清除指定缓存
        /// </summary>
        /// <param name="type">1:内存 2:文件</param>
        public static void RemoveAllCache(int type)
        {
            if (type == 1)
            {
                MemoryCache cache = System.Runtime.Caching.MemoryCache.Default;
                var dictionary = cache.GetValues(null);
                if (dictionary != null)
                {
                    foreach (var c in dictionary)
                    {
                        cache.Remove(c.Key);
                    }
                }
            }
            else if (type == 2)
            {
                DirectoryInfo di = new DirectoryInfo(FilePath);
                di.Delete(true);
            }
        }

        #region 流序列化
        /// <summary>
        /// 流序列化
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="obj"></param>
        public static void BinarySerialize(Stream stream, object obj)
        {
            try
            {
                stream.Seek(0, SeekOrigin.Begin);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
            }
            catch (Exception e)
            {
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            finally
            {
                stream.Dispose();
            }
        }

        /// <summary>
        /// 流序列化
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object BinaryDeSerialize(Stream stream)
        {
            object obj = null;
            stream.Seek(0, SeekOrigin.Begin);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                obj = formatter.Deserialize(stream);
            }
            catch (Exception e)
            {
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            finally
            {
                stream.Dispose();
            }
            return obj;
        }
        #endregion
    }


}
