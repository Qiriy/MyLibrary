﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace ETT.Library.ETTHelper.CCache
{
    /// <summary>
    /// 缓存对象数据结构
    /// </summary>
    [Serializable()]
    public class CacheData
    {
        public object Value { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTimeOffset AbsoluteExpiration { get; set; }

        public DateTime FailureTime
        {
            get
            {
                if (AbsoluteExpiration == System.Runtime.Caching.ObjectCache.InfiniteAbsoluteExpiration)
                {
                    return AbsoluteExpiration.DateTime;
                }
                else
                {
                    return CreateTime.AddTicks(AbsoluteExpiration.Ticks);
                }
            }
        }

        public CacheItemPriority Priority { get; set; }
    }
}
