﻿using System;
using System.IO;
using System.Text;
using ETT.Library.ETTHelper.LogHelper;

namespace ETT.Library.ETTHelper.EncryptLibaray
{
    /// <summary>
    /// MD5加密/解密类
    /// </summary>
    public class MD5Encrypt
    {
        /// <summary>
        /// MD5实现不可逆加密
        /// </summary>
        /// <param name="str">需要加密的字符串</param>
        /// <param name="i">[选填] 为加密后加密字符串的长度 默认为32位</param>
        /// <returns></returns>
        public static string Encrypt(string str, MD5Enum i = MD5Enum.INT)
        {
            string result;
            try
            {
                //获取要加密的字段，并转化为Byte[]数组
                byte[] data = System.Text.Encoding.Unicode.GetBytes(str.ToCharArray());
                //建立加密服务
                System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                //加密Byte[]数组
                //将加密后的数组转化为字段
                if ((int)i == 16 && str != string.Empty)
                {
                    result = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str, "MD5").ToLower().Substring(8, 16);
                }
                else if ((int)i == 32 && str != string.Empty)
                {
                    result = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str, "MD5").ToLower();
                    //result = new Guid(result).ToString();
                }
                else
                {
                    switch (i)
                    {
                        case MD5Enum.SORT: result = MD5_DEFUALT_16; break;
                        case MD5Enum.INT: result = MD5_DEFUALT;
                            //result = new Guid(result).ToString();
                            break;
                        default: result = MD5_ERROR_ALERTSTRING; break;
                    }

                }
            }
            catch (Exception e)
            {
                result = string.Empty;
                WriteError.WriteErrorLog(e.Message);
            }
            return result;
        }

        #region<<获取文件MD5值>>

        /// <summary>
        /// 获取文件的唯一MD5值
        /// </summary>
        /// <param name="filename">文件路径</param>
        /// <returns></returns>
        public static string GetMd5HashFromFile(string filename)
        {
            using (FileStream file = new FileStream(filename, FileMode.Open))
            {
                try
                {
                    System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                    byte[] retVal = md5.ComputeHash(file);
                    file.Close();

                    StringBuilder sb = new StringBuilder();
                    foreach (byte t in retVal)
                    {
                        sb.Append(t.ToString("x2"));
                    }
                    return sb.ToString();
                }
                catch (Exception ex)
                {
                    WriteError.WriteErrorLog(ex.Message);
                    return MD5_DEFUALT;
                }
            }
        }


        #region<<常量>>

        /// <summary>
        /// <para>值  ：00000000000000000000000000000000</para>
        /// <para>描述：为默认MD5值</para>
        /// </summary>
        private const string MD5_DEFUALT = "00000000000000000000000000000000";

        /// <summary>
        /// <para>值  ：000000000000000</para>
        /// <para>描述：为默认16位MD5值</para>
        /// </summary>
        private const string MD5_DEFUALT_16 = "000000000000000";

        /// <summary>
        /// <para>值  ：请确保调用函数时第二个参数选择枚举为32位或16位</para>
        /// <para>描述：MD5加密错误时出现的错误提示</para>
        /// </summary>
        private const string MD5_ERROR_ALERTSTRING = @"请确保调用函数时第二个参数选择枚举为32位或16位";

        #endregion

        #endregion

    }
}
