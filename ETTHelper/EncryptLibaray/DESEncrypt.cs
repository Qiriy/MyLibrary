﻿using System;
using System.Security.Cryptography;
using System.Text;
using ETT.Library.ETTHelper.LogHelper;

namespace ETT.Library.ETTHelper.EncryptLibaray
{
    /// <summary>
    /// DES加密/解密类
    /// </summary>
    public class DESEncrypt
    {
        /// <summary>
        /// 根据密钥加密指定字符串
        /// </summary>
        /// <param name="str">需要加密的字符串</param>
        /// <param name="key">[选填] 加密密钥，如果不填，默认为 YITAITONG</param>
        /// <returns></returns>
        public static string Encrypt(string str, string key = null)
        {
            string result;
            try
            {
                if (string.IsNullOrWhiteSpace(key))
                {
                    key = NormalString.ENCRYPTKEY;
                }
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray;
                inputByteArray = Encoding.Default.GetBytes(str);
                des.Key = Encoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(key, "md5").Substring(0, 8));
                des.IV = Encoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(key, "md5").Substring(0, 8));
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                StringBuilder ret = new StringBuilder();
                foreach (byte b in ms.ToArray())
                {
                    ret.AppendFormat("{0:X2}", b);
                }
                result = ret.ToString();
            }
            catch (Exception e)
            {
                WriteError.WriteErrorLog(e.Message);
                result = str;
            }
            return result;
        }

        /// <summary>
        /// 根据密钥解密指定字符串
        /// </summary>
        /// <param name="str">需要解密的字符串</param>
        /// <param name="key">[选填] 解密密钥，如果不填，默认为 YITAITONG</param>
        public static string Decrypt(string str, string key = null)
        {
            string result;
            try
            {
                if (string.IsNullOrWhiteSpace(key))
                {
                    key = NormalString.ENCRYPTKEY;
                }
                else if (key.Length < 8)
                {
                    key += NormalString.ENCRYPTKEY;
                }
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                int len;
                len = str.Length / 2;
                byte[] inputByteArray = new byte[len];
                int x, i;
                for (x = 0; x < len; x++)
                {
                    i = Convert.ToInt32(str.Substring(x * 2, 2), 16);
                    inputByteArray[x] = (byte)i;
                }
                des.Key = Encoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(key, "md5").Substring(0, 8));
                des.IV = Encoding.ASCII.GetBytes(System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(key, "md5").Substring(0, 8));
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                result = Encoding.Default.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                WriteError.WriteErrorLog(e.Message);
                result = str;
            }
            return result;
        }
    }
}
