﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETT.Library.ETTHelper.CWin32
{
    /// <summary>
    /// Win32 参数
    /// </summary>
    public static class Win32Paramters
    {
        /// <summary>
        /// 读写
        /// </summary>
        public const int OF_READWRITE = 2;
        /// <summary>
        /// 共享/拒绝/不存在
        /// </summary>
        public const int OF_SHARE_DENY_NONE = 0x40;
        /// <summary>
        /// 文件错误
        /// </summary>
        public static readonly IntPtr HFILE_ERROR = new IntPtr(-1);  
    }
}
