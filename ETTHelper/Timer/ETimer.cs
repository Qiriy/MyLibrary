﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ETT.Library.ETTHelper.Timer
{
    /// <summary>
    /// ETT计时器帮助类
    /// </summary>
    public class ETimer
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="time">有效时间（秒）</param>
        public ETimer(int time)
        {
            _exsittime = time;
        }

        private System.Timers.Timer aTimer;

        /// <summary>
        /// 初始化计时类
        /// </summary>
        public void InitTimer()
        {
            aTimer = new System.Timers.Timer(_exsittime);
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 10;
            aTimer.Enabled = true;
        }

        public bool ResultOfTimer()
        {
            while (!_isexist)
            {
                ResultOfTimer();
            }
            return _isexist;
        }

        private  void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            _isexist = true;
        }

        /// <summary>
        /// 私有字段 有效期默认 4分钟
        /// </summary>
        public int ExsitTime 
        {
            get { return _exsittime; }
        }

        /// <summary>
        /// 私有字段 有效期默认 4分钟
        /// </summary>
        private int _exsittime = 240;

        /// <summary>
        /// 是否过期
        /// </summary>
        public bool IsExist
        {
            get { return _isexist; }
        }

        /// <summary>
        /// 是否过期 默认为未过期 false
        /// </summary>
        private bool _isexist = false;
    }
}
