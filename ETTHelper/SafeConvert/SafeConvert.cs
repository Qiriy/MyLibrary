﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ETT.Library.ETTHelper.LogHelper;

namespace ETT.Library.ETTHelper
{
    /// <summary>
    /// 安全转换类 
    /// </summary>
    public static class SafeConvert
    {
        /// <summary>
        /// 将时间转换成指定格式的字符串[扩展方法]
        /// </summary>
        /// <param name="dt">时间</param>
        /// <param name="strfomart">时间格式</param>
        /// <returns>输入时间格式的字符串</returns>
        public static string ToSafeString(this DateTime dt, string strfomart)
        {
            var str = string.Empty;
            try
            {
                str = dt.ToString(strfomart);
            }
            catch
            {
                str = dt.ToString(CultureInfo.InvariantCulture);
            }
            return str;
        }

        /// <summary>
        /// 将Obj类型转换成String类型[扩展方法]
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ToSafeString(this object item)
        {
            string str;
            try
            {
                str = item.ToString();
            }
            catch
            {
                str = null;
            }
            return str;
        }

        /// <summary>
        /// 将Object类型转换成Int32类型
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Int32 ToInt32(object obj)
        {
            Int32 i = 0;
            try
            {
                i = Convert.ToInt32(obj);
            }
            catch
            {
                i = 0;
            }
            return i;
        }


        /// <summary>
        /// 将object 类型转换成 32位整形或者null
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Int32? ToInt32OrNull(object obj)
        {
            Int32? i = 0;
            try
            {
                i = Convert.ToInt32(obj);
            }
            catch
            {
                i = null;
            }
            return i;
        }


        /// <summary>
        /// 将Object类型转换成Int16类型
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Int16 ToInt16(object obj)
        {
            Int16 i = 0;
            try
            {
                i = Convert.ToInt16(obj);
            }
            catch
            {
                i = 0;
            }
            return i;
        }

        /// <summary>
        /// 将object 类型转换成 16位整形或者null
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Int16? ToInt16OrNull(object obj)
        {
            Int16? i = 0;
            try
            {
                i = Convert.ToInt16(obj);
            }
            catch
            {
                i = null;
            }
            return i;
        }

        /// <summary>
        /// 将Object类型转换成Int64类型
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Int64 ToInt64(object obj)
        {
            Int64 i = 0;
            try
            {
                i = Convert.ToInt64(obj);
            }
            catch
            {
                i = 0;
            }
            return i;
        }

        /// <summary>
        /// 将object 类型转换成 64位整形或者null
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Int64? ToInt64OrNull(object obj)
        {
            Int64? i = 0;
            try
            {
                i = Convert.ToInt64(obj);
            }
            catch
            {
                i = null;
            }
            return i;
        }

        /// <summary>
        /// 将Object类型转换成Decimal类型
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Decimal ToDecimal(object obj)
        {
            Decimal i = 0;
            try
            {
                i = Convert.ToDecimal(obj);
            }
            catch
            {
                i = 0;
            }
            return i;
        }

        /// <summary>
        /// 将object 类型转换成 Decimal类型或者null
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Decimal? ToDecimalOrNull(object obj)
        {
            Decimal? i = 0;
            try
            {
                i = Convert.ToDecimal(obj);
            }
            catch
            {
                i = null;
            }
            return i;
        }


        /// <summary>
        /// 将Object类型转换成Double类型
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Double ToDouble(object obj)
        {
            Double i = 0;
            try
            {
                i = Convert.ToDouble(obj);
            }
            catch
            {
                i = 0;
            }
            return i;
        }

        /// <summary>
        /// 将object 类型转换成 Double类型或者null
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Double? ToDoubleOrNull(object obj)
        {
            Double? i = 0;
            try
            {
                i = Convert.ToDouble(obj);
            }
            catch
            {
                i = null;
            }
            return i;
        }


        /// <summary>
        /// 将Object类型转换成单精度浮点型
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Single ToSingle(object obj)
        {
            Single i = 0;
            try
            {
                i = Convert.ToSingle(obj);
            }
            catch
            {
                i = 0;
            }
            return i;
        }


        /// <summary>
        /// 将object 类型转换成 单精度浮点型或者null
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static Single? ToSingleOrNull(object obj)
        {
            Single? i = 0;
            try
            {
                i = Convert.ToSingle(obj);
            }
            catch
            {
                i = null;
            }
            return i;
        }

        /// <summary>
        /// 将时间字符串转换成DateTime类型
        /// </summary>
        /// <param name="strdate">时间字符串</param>
        /// <param name="msg">是否转换成功</param>
        /// <returns></returns>
        public static DateTime ToDateTime(object strdate, ref bool msg)
        {
            DateTime dt;
            try
            {
                dt = DateTime.Parse(strdate.ToString());
                msg = true;
            }
            catch
            {
                msg = false;
                dt = new DateTime();
            }
            return dt;
        }

        /// <summary>
        /// 将时间字符串转换成DateTime类型或者NULL
        /// </summary>
        /// <param name="strdate">时间字符串</param>
        /// <returns></returns>
        public static DateTime? ToDateTimeOrNull(object strdate)
        {
            DateTime? dt;
            try
            {
                dt = DateTime.Parse(strdate.ToString());
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        /// <summary>
        /// 将字符集转换成BASE64字符串
        /// </summary>
        /// <param name="bytes">字符集</param>
        /// <returns></returns>
        public static string ToBase64String(Byte[] bytes)
        {
            string item;
            try
            {
                item = Convert.ToBase64String(bytes);
            }
            catch
            {
                item = string.Empty;
            }
            return item;
        }

        /// <summary>
        /// 由Base64转换成字节数组
        /// </summary>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public static Byte[] ToByteFromBase64(string base64String)
        {
            Byte[] bytes;
            try
            {
                bytes = Convert.FromBase64String(base64String);
            }
            catch
            {
                bytes = new byte[2048];
            }
            return bytes;
        }

        /// <summary>
        /// 将object类型转换成布尔类型或者null
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static bool? ToBooleanOrNull(object obj)
        {
            bool? item;
            try
            {
                item = Convert.ToBoolean(obj);
            }
            catch
            {
                item = null;
            }
            return item;
        }


        /// <summary>
        /// 将object类型转换成布尔类型
        /// </summary>
        /// <param name="obj">object类型数据</param>
        /// <returns></returns>
        public static bool ToBoolean(object obj)
        {
            bool item;
            try
            {
                item = Convert.ToBoolean(obj);
            }
            catch
            {
                item = false;
            }
            return item;
        }

        /// <summary>
        /// 序列化后Model转换工具，不支持含有集合的类
        /// </summary>
        /// <typeparam name="T">类1</typeparam>
        /// <typeparam name="TU">类2</typeparam>
        /// <param name="t">对象1</param>
        /// <param name="u">对象2</param>
        /// <returns></returns>
        public static bool ToGeneric<T, TU>(T t, out TU u) where TU : class , new()
        {
            u = new TU();
            try
            {
                var propertys = t.GetType().GetProperties();
                foreach (var item in propertys)
                {
                    if (item.PropertyType.Name.ToLower().Contains("list") ||
                        item.PropertyType.Name.ToLower().Contains("queryable"))
                    {
                        var name = item.Name;
                        dynamic a = item.GetValue(t, null);
                    }
                    else
                    {
                        var a = u.GetType().GetProperty(item.Name);
                        a.SetValue(u, item.GetValue(t, null), null);
                    }
                }

            }
            catch (Exception e)
            {
                WriteLog.WriteSystemLog(e.Message);
                return false;
            }
            return true;
        }
    }
}
