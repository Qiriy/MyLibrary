﻿using System;

namespace ETT.Library.ETTHelper
{
    /// <summary>
    /// 路径字符串
    /// </summary>
    public class PathString
    {
        /// <summary>
        /// <para>值：当前目录（即该进程从中启动的目录）的完全限定路径</para>
        /// <para>描述： log后缀日志文件存放路径</para>
        /// </summary>
        public static readonly string LogFilePath = string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["LogPath"]) ? Environment.CurrentDirectory + NormalString.NORMALDATELOGPREFIX + DateTime.Now.ToString(NormalString.NORMALDATELOGSTRING) : System.Configuration.ConfigurationManager.AppSettings["LogPath"] + "\\" +  DateTime.Now.ToString(NormalString.NORMALDATELOGSTRING);

        /// <summary>
        /// <para>值：当前目录（即该进程从中启动的目录）的完全限定路径</para>
        /// <para>描述：错误日志txt记录路径</para>
        /// </summary>
        public static readonly string ErrorFilePath = string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ErrorPath"]) ? Environment.CurrentDirectory + NormalString.NORMALDATEERRORPREFIX + DateTime.Now.ToString(NormalString.NORMALDATELOGSTRING) : System.Configuration.ConfigurationManager.AppSettings["ErrorPath"] + "\\" + DateTime.Now.ToString(NormalString.NORMALDATELOGSTRING);

    }
}
