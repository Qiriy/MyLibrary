﻿namespace ETT.Library.ETTHelper
{
    /// <summary>
    /// 常用静态变量
    /// </summary>
    internal class NormalString
    {
        /// <summary>
        /// <para>值：yyyyMMdd</para>
        /// <para>描述：记录log等时使用的时间格式</para>
        /// </summary>
        internal static string NORMALDATELOGSTRING = "yyyyMMdd";

        /// <summary>
        /// <para>值：\SystemLog\</para>
        /// <para>描述：记录log等时使用的前缀路径</para>
        /// </summary>
        internal static string NORMALDATELOGPREFIX = "\\SystemLog\\";

        /// <summary>
        /// <para>值：\SystemLog\</para>
        /// <para>描述：记录错误信息时使用的前缀路径</para>
        /// </summary>
        internal static string NORMALDATEERRORPREFIX = "\\ErrorLog\\";

        /// <summary>
        /// <para>值：yyyy-MM-dd HH:mm:ss</para>
        /// <para>描述：时间转换字符串格式时使用的时间格式</para>
        /// </summary>
        internal static string NORMALDTAESTRING = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// <para>值：yyyy-MM-dd</para>
        /// <para>描述：时间转换字符串格式时使用的时间格式</para>
        /// </summary>
        internal static string NORMALSAMLLDTAESTRING = "yyyy-MM-dd";

        /// <summary>
        /// <para>值：\Systemlog_{0}.log</para>
        /// <para>描述：系统日志文件名称</para>
        /// </summary>
        internal static string LOGFILENAME = "\\Systemlog_{0}.log";

        /// <summary>
        /// <para>值：\SystemError_{0}.log</para>
        /// <para>描述：错误日志文件名称</para>
        /// </summary>
        internal static string ERRORFILENAME = "\\SystemError_{0}.txt";

        /// <summary>
        /// <para>值：YITAITONG</para>
        /// <para>描述：加密/解密默认密钥</para>
        /// </summary>
        internal static string ENCRYPTKEY = @"YITAITONG";
    }
}
