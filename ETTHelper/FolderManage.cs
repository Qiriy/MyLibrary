﻿using System;
using System.IO;

namespace ETT.Library.ETTHelper
{
    /// <summary>
    /// 文件夹管理类
    /// </summary>
    public static class FolderManage
    {

        /// <summary>
        /// 文件夹路径是否存在,如果不存在则创建文件夹
        /// </summary>
        /// <param name="path">文件夹路径</param>
        /// <param name="fileenum">文件及文件夹枚举[用于判断文件夹处理机制]</param>
        /// <returns></returns>
        public static bool IsFolderExist(string path, FileEnum fileenum = FileEnum.None)
        {
            bool bresult = false;
            try
            {
                if (!Directory.Exists(path))
                {
                    var msg = string.Empty;
                    if (fileenum == FileEnum.Create)
                    {
                        bresult = CreateFolder(path, ref msg);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return bresult;
        }

        /// <summary>
        /// 创建文件夹路径
        /// </summary>
        /// <param name="path">文件夹路径</param>
        /// <param name="msg">文件夹路径</param>
        /// <returns></returns>
        public static bool CreateFolder(string path, ref string msg)
        {
            var bResult = false;
            try
            {
                Directory.CreateDirectory(path);
                bResult = true;
            }
            catch (Exception e)
            {
                throw e;
            }
            return bResult;
        }
    }
}
