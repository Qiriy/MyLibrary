﻿using ETT.Library.ETTHelper;
using System;
using System.Web;

namespace ETT.Library.ETTHelper.WEBHelper.WEBMemory
{
    /// <summary>
    /// Cookie管理类
    /// </summary>
    public class Cookies
    {
        /// <summary>
        /// 添加Cookie
        /// </summary>
        /// <param name="cname">cookie名</param>
        /// <param name="cvalue">cookie 值</param>
        /// <param name="ctime">过期时间[默认为 2小时过期]</param>
        public static void SetCookie(string cname, string cvalue, int ctime = 120)
        {
            HttpCookie cookie = new HttpCookie(cname, cvalue);
            DateTime dt = DateTime.Now;
            cookie.Expires = DateTime.Now.AddMinutes(ctime);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// 获取某个Cookie有效期还剩余多少时间
        /// </summary>
        /// <param name="cname">cookie名</param>
        /// <returns></returns>
        public static int GetCookieExpiresTime(string cname)
        {
            int expires = 0;
            HttpCookie cookie = HttpContext.Current.Request.Cookies[cname];
            if (cookie != null)
            {
                var sp = cookie.Expires - DateTime.Now;
                expires = SafeConvert.ToInt32(Math.Round(sp.TotalMinutes,0));
            }
            return expires;
        }

        /// <summary>
        /// 清除指定Cookie
        /// </summary>
        /// <param name="cookiename">cookie 名</param>
        public static void ClearCookie(string cookiename)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookiename];
            if (cookie == null) return;
            cookie.Expires = DateTime.Now.AddYears(-3);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// 获取指定Cookie值
        /// </summary>
        /// <param name="cookiename">cookiename</param>
        /// <returns></returns>
        public static string GetCookieValue(string cookiename)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[cookiename];
            string str = string.Empty;
            if (cookie != null)
            {
                str = cookie.Value;
            }
            return str;
        }

    }
}
