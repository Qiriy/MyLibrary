﻿using System;
using System.Web;
using ETT.Library.ETTHelper.LogHelper;
using ETT.Library.ETTHelper.StringLibaray;

namespace ETT.Library.ETTHelper.WEBHelper.WEBMemory
{
    /// <summary>
    /// Session管理类：需要引用ChnCharInfo.dll
    /// </summary>
    public class CSessions
    {
        /// <summary>
        /// 添加Session
        /// </summary>
        /// <param name="sessionName">Session对象名称</param>
        /// <param name="path">Session路径</param>
        /// <param name="value">Session值</param>
        /// <param name="iExpires">调动有效期（分钟）</param>
        public static void Add(string sessionName, string path, object value, int iExpires = 120)
        {
            path = SessionPathNameChange(path);
            HttpContext.Current.Session[sessionName + "_" + path] = value;
            HttpContext.Current.Session.Timeout = iExpires;
        }

        /// <summary>
        /// 转换Session路径中的特殊字符为_并将汉字转换成拼音（如果引用dll）
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string SessionPathNameChange(string path)
        {
            path = string.IsNullOrWhiteSpace(path) ? string.Empty : path;
            path=  path.Replace(':', '_').Replace("\"", "_").Replace("\\", "_")
                .Replace("/", "_").Replace("?", "_").Replace(".", "_")
                .Replace("<", "_").Replace(">", "_").Replace("=", "_")
                .Replace("\'", "_").Replace("“", "_").Replace("”", "_")
                .Replace("《", "_").Replace("》", "_").Replace("。", "_")
                .Replace("，", "_").Replace(",", "_").Replace("？", "_");
            try
            {
                if (_isQuote)
                {
                    path = path.GetQuanPin();
                }
            }
            catch(Exception ex)
            {
                _isQuote = false;
                WriteError.WriteErrorLog("未引用ChnCharInfo.dll，将不转换成拼音！");
            }
            return path;
        }

        /// <summary>
        /// 添加Session
        /// </summary>
        /// <param name="sessionName">Session对象名称</param>
        /// <param name="path">路径</param>
        /// <param name="values">Session值数组</param>
        /// <param name="iExpires">调动有效期（分钟）</param>
        public static void Adds(string sessionName, string path, object[] values, int iExpires = 120)
        {
            path = SessionPathNameChange(path);
            HttpContext.Current.Session[sessionName + "_" + path] = values;
            HttpContext.Current.Session.Timeout = iExpires;
        }

        /// <summary>
        /// 添加Session
        /// </summary>
        /// <param name="sname">Session对象名称</param>
        /// <param name="path">路径</param>
        /// <param name="obj">Session对象</param>
        /// <param name="iExpires">调动有效期(分钟)</param>
        public static void AddO(string sname, string path, object obj, int iExpires = 120)
        {
            path = SessionPathNameChange(path);
            HttpContext.Current.Session[sname + "_" + path] = obj;
            HttpContext.Current.Session.Timeout = iExpires;
        }

        /// <summary>
        /// 读取某个Session对象值
        /// </summary>
        /// <param name="strSessionName">Session对象名称</param>
        /// <param name="path">路径</param>
        /// <returns>Session对象值</returns>
        public static object Get(string strSessionName, string path)
        {
            path = SessionPathNameChange(path);
            if (HttpContext.Current.Session[strSessionName + "_" + path] == null)
            {
                return null;
            }
            else
            {
                return HttpContext.Current.Session[strSessionName + "_" + path];
            }
        }


        /// <summary>
        /// 读取某个Session对象值数组
        /// </summary>
        /// <param name="strSessionName">Session对象名称</param>
        /// <param name="path">路径</param>
        /// <returns>Session对象值数组</returns>
        public static object[] Gets(string strSessionName, string path)
        {
            path = SessionPathNameChange(path);
            if (HttpContext.Current.Session[strSessionName + "_" + path] == null)
            {
                return null;
            }
            else
            {
                return (object[])HttpContext.Current.Session[strSessionName + "_" + path];
            }
        }


        /// <summary>
        /// 读取某个Session对象
        /// </summary>
        /// <param name="strSessionName">Session对象名称</param>
        /// <param name="path">路径</param>
        /// <returns>Session对象值数组</returns>
        public static object Geto(string strSessionName, string path)
        {
            path = SessionPathNameChange(path);
            return HttpContext.Current.Session[strSessionName + "_" + path] ?? null;
        }

        /// <summary>
        /// 删除某个Session对象
        /// </summary>
        /// <param name="strSessionName">Session对象名称</param>
        /// <param name="path">路径</param>
        public static void ClearSession(string strSessionName, string path)
        {
            path = SessionPathNameChange(path);
            HttpContext.Current.Session[strSessionName + "_" + path] = null;
        }

        /// <summary>
        /// 是否引用dll
        /// </summary>
        private static bool _isQuote = true;
    }
}
