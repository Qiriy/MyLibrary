﻿using ETT.Library.ETTHelper.WebHelper.WEBEnum;

namespace ETT.Library.ETTHelper.WEBHelper.WEBMemory
{
    /// <summary>
    /// 混合模式
    /// </summary>
    public class CBlend
    {
        /// <summary>
        /// 添加混合模式缓存记录
        /// </summary>
        /// <param name="cname"></param>
        /// <param name="cvalue"></param>
        /// <param name="iExpires"></param>
        public static void AddMemory(string cname, object cvalue, int iExpires = 120)
        {
            Cookies.SetCookie(cname, cname);//可设置时间限制
            CSessions.AddO(cname, null, cvalue, iExpires);
        }

        /// <summary>
        /// 清除缓存记录
        /// </summary>
        /// <param name="cname"></param>
        public static void ClearMemory(string cname)
        {
            Cookies.ClearCookie(cname);
            CSessions.ClearSession(cname, null);
        }

        /// <summary>
        /// 找回丢失的session
        /// </summary>
        /// <param name="cname">缓存名称</param>
        /// <param name="obj"></param>
        /// <param name="en"></param>
        public static void GetBackMemory(string cname, object obj, ref MemoryEnum en)
        {
            en = MemoryEnum.RESET;
            int time = Cookies.GetCookieExpiresTime(cname);
            if (time > 0)
            {
                CSessions.AddO(cname, null, obj, time);
            }
            else
            {
                en = MemoryEnum.OUTDATE;
            }
        }
    }
}
