﻿using System.ComponentModel;

namespace ETT.Library.ETTHelper.WebHelper.WEBEnum
{
    /// <summary>
    /// 缓存枚举
    /// </summary>
    public enum MemoryEnum
    {
        /// <summary>
        /// 丢失
        /// </summary>
        [Description("丢失")]
        LOSE = 0,
        /// <summary>
        /// 重置
        /// </summary>
        [Description("重置")]
        RESET = 1,
        /// <summary>
        /// 过期
        /// </summary>
        [Description("过期")]
        OUTDATE = 2,
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        NORMAL = 3
    }
}
