﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.ETTHelper.CXML
{
    /// <summary>
    /// XML帮助类
    /// </summary>
    public class CXMLHelper
    {
        /// <summary>
        /// 获取XML字符串
        /// </summary>
        /// <typeparam name="T">实体类</typeparam>
        /// <param name="t">对象</param>
        /// <returns></returns>
        public static string GetXmlString<T>(T t) where T : class, new()
        {
            string xml = String.Empty;
            StringBuilder str = new StringBuilder();
            str.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
            try
            {
                if (t == null)
                {
                    t = new T();
                }
                var ttype = t.GetType();
                var props = ttype.GetProperties();
                str.AppendFormat("<{0}>\r\n", ttype.Name);
                foreach (var item in props)
                {
                    str.AppendFormat(XMLNODES_FORMAT, item.Name, item.GetValue(t, null));
                }
                str.AppendFormat("</{0}>\r\n", ttype.Name);
            }
            catch (Exception e)
            {
                xml = ERROR_XML_MSG;
                LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            return xml;
        }

        /// <summary>
        /// <para>值：</para>
        /// <para>描述：</para>
        /// </summary>
        private readonly static string ERROR_XML_MSG = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<ERROR>解析MODEL过程中过程中发生错误</ERROR>";

        /// <summary>
        /// <para>值：<![CDATA[<]]>{0}><![CDATA[{1}]]><![CDATA[<]]>/{0}> \r\n</para>
        /// <para>描述：转成XML时的内容</para>
        /// </summary>
        private static readonly string XMLNODES_FORMAT = "<{0}><![CDATA[{1}]]></{0}> \r\n";
    }
}
