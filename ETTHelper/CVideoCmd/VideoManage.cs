﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ETT.Library.ETTHelper.CVideoCmd
{
    /// <summary>
    /// 视频管理模块
    /// </summary>
    public class VideoManage
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public VideoManage()
        {
        }

        /// <summary>
        /// 名称标识
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string _arg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string _cmsg { get; set; }

        Process p = new Process();//建立外部调用线程
        /// <summary>
        /// 是否退出
        /// </summary>
        private bool _isQuit = false;
        /// <summary>
        /// 运行状态
        /// </summary>
        private bool _isRunning = false;
        /// <summary>
        /// 执行操作
        /// </summary>
        /// <param name="arguments"></param>
        /// <param name="msg">[输出] 消息提示 </param>
        public void ConvertVideo(string arguments, out string msg)
        {
            msg = string.Empty;
            p.StartInfo.FileName = "cmd.exe";//ConfigurationManager.AppSettings["FFmpegFilePath"];//要调用外部程序的绝对路径 Appsettings FFmpegFilePath @"c:/ffmpeg.exe"
            p.StartInfo.WorkingDirectory = @"C:\Windows\System32\cmd.exe";
            p.StartInfo.Arguments = arguments;//参数(这里就是FFMPEG的参数了)
            p.StartInfo.UseShellExecute = false;//不使用操作系统外壳程序启动线程(一定为FALSE,详细的请看MSDN)
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;//把外部程序错误输出写到StandardError流中(这个一定要注意,FFMPEG的所有输出信息,都为错误输出流,用StandardOutput是捕获不到任何消息的...这是我耗费了2个多月得出来的经验...mencoder就是用standardOutput来捕获的)
            p.StartInfo.CreateNoWindow = false;//不创建进程窗口
            p.ErrorDataReceived += new DataReceivedEventHandler(Output);//外部程序(这里是FFMPEG)输出流时候产生的事件,这里是把流的处理过程转移到下面的方法中,详细请查阅MSDN
            p.Start();//启动线程
            _isRunning = true;
            p.StandardInput.WriteLine(arguments);
            p.BeginErrorReadLine();//开始异步读取
            p.WaitForExit();//阻塞等待进程结束

        }

        /// <summary>
        /// 执行操作
        /// </summary>
        public void ConvertVideo()
        {
            _cmsg = string.Empty;
            p.StartInfo.FileName = "cmd.exe";//ConfigurationManager.AppSettings["FFmpegFilePath"];//要调用外部程序的绝对路径 Appsettings FFmpegFilePath @"c:/ffmpeg.exe"
            p.StartInfo.Arguments = _arg;//参数(这里就是FFMPEG的参数了)
            p.StartInfo.UseShellExecute = false;//不使用操作系统外壳程序启动线程(一定为FALSE,详细的请看MSDN)
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;//把外部程序错误输出写到StandardError流中(这个一定要注意,FFMPEG的所有输出信息,都为错误输出流,用StandardOutput是捕获不到任何消息的...这是我耗费了2个多月得出来的经验...mencoder就是用standardOutput来捕获的)
            p.StartInfo.CreateNoWindow = false;//不创建进程窗口
            p.ErrorDataReceived += new DataReceivedEventHandler(Output);//外部程序(这里是FFMPEG)输出流时候产生的事件,这里是把流的处理过程转移到下面的方法中,详细请查阅MSDN
            p.Start();//启动线程
            _isRunning = true;
            p.StandardInput.WriteLine(_arg);
            p.BeginErrorReadLine();//开始异步读取
            p.WaitForExit();//阻塞等待进程结束
        }
        private void Output(object sendProcess, DataReceivedEventArgs output)
        {
            if (!String.IsNullOrEmpty(output.Data))
            {
                if (!_isQuit)
                {
                    ETT.Library.ETTHelper.LogHelper.WriteLog.WriteSystemLog(output.Data);
                    Console.WriteLine(output.Data);
                }
                else
                {
                    try
                    {
                        p.StandardInput.WriteLine("q");
                        //p.StandardInput.Write("q");
                    }
                    catch
                    {
                    }
                    finally
                    {
                        _isRunning = false;
                        if (p != null && !p.HasExited)
                        {
                            p.Kill();
                            p.Close();//关闭进程
                            p.Dispose();//释放资源
                            p = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 停止录制
        /// </summary>
        /// <returns></returns>
        public bool CloseVideo()
        {
            _isQuit = true;
            return !_isRunning;
        }

    }
}
