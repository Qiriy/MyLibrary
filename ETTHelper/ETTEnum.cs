﻿namespace ETT.Library.ETTHelper
{
    /// <summary>
    /// 文件及文件夹管理枚举
    /// </summary>
    public enum FileEnum
    {
        /// <summary>
        /// 默认
        /// </summary>
        None = 0,
        /// <summary>
        /// 创建
        /// </summary>
        Create = 1,
        /// <summary>
        /// 仅判断存不存在
        /// </summary>
        IsExist = 2,
        /// <summary>
        /// 添加内容
        /// </summary>
        Add = 3
    }

    /// <summary>
    /// MD5加密枚举
    /// </summary>
    public enum MD5Enum
    {
        /// <summary>
        /// 十六位
        /// </summary>
        SORT = 16,
        /// <summary>
        /// 32位
        /// </summary>
        INT = 32
    }
}
