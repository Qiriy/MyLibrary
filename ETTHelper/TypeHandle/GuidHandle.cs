﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.ETTHelper.TypeHandle
{
    /// <summary>
    /// GUID处理类
    /// </summary>
    public static class GuidHandle
    {
        /// <summary>
        /// true GUID 为 0000000-0000-0000-000000000000 
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static bool IsEmpty(this Guid guid)
        {
            return guid == Guid.Empty;
        }

        /// <summary>
        /// stirng 转换成GUID
        /// </summary>
        /// <param name="guid">字符串</param>
        /// <returns></returns>
        public static Guid ToGuid(this string guid)
        {
            return new Guid(guid);
        }
    }
}
