﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using ETT.Library.ETTHelper.CWin32;

namespace ETT.Library.ETTHelper
{
    /// <summary>
    /// 文件管理类
    /// </summary>
    public static class FileManage
    {
        /// <summary>
        /// 判断文件路径文件是否存在
        /// </summary>
        /// <param name="item">文件夹路径</param>
        /// <param name="fileenum">文件及文件夹枚举[用于判断文件夹处理机制]</param>
        /// <returns>返回文件是否存在</returns>
        public static bool IsExistFile(string item, FileEnum fileenum = FileEnum.None)
        {
            bool bresult = false;
            try
            {
                if (!File.Exists(item))
                {
                    bresult = false;
                    var msg = string.Empty;
                    if (fileenum == FileEnum.Create)
                    {
                        bresult = CreateFile(item, ref msg);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return bresult;
        }

        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="path">文件的全部路径</param>
        /// <param name="msg">[返回]消息提示</param>
        /// <returns></returns>
        public static bool CreateFile(string path, ref string msg)
        {
            bool bresult = false;
            try
            {
                using (FileStream fs = File.Create(path))
                {
                    fs.Close();
                    bresult = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return bresult;
        }

        /// <summary>
        /// 向系统日志文件中添加内容
        /// </summary>
        /// <param name="path">文件地址</param>
        /// <param name="str">输入内容</param>
        public static void AppendLog(string path, string str)
        {
            using (StreamWriter sr = new StreamWriter(path, true))
            {
                try
                {
                    sr.WriteLine(string.Format(@"[{0}] 系统记录日志 ：{1} ", DateTime.Now.ToString(NormalString.NORMALDTAESTRING), str));
                    sr.Dispose();
                }
                catch (Exception ex)
                {
                    sr.Dispose();
                    throw;
                }
            }
        }

        /// <summary>
        /// 查看文件是否被占用
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsFileOccupied(string filePath)
        {
            IntPtr vHandle = Win32Method._lopen(filePath, Win32Paramters.OF_READWRITE | Win32Paramters.OF_SHARE_DENY_NONE);
            Win32Method.CloseHandle(vHandle);
            return vHandle == Win32Paramters.HFILE_ERROR ? true : false;
        }

        /// <summary>
        /// 解除文件占用
        /// </summary>
        /// <param name="fileName"></param>
        public static void EndUsing(string fileName)
        {
            using (Process tool = new Process())
            {
                tool.StartInfo.FileName = "handle.exe";
                tool.StartInfo.Arguments = fileName + " /accepteula";
                tool.StartInfo.UseShellExecute = false;
                tool.StartInfo.RedirectStandardOutput = true;
                tool.Start();
                tool.WaitForExit();
                var outputTool = tool.StandardOutput.ReadToEnd();

                var matchPattern = @"(?<=\s+pid:\s+)\b(\d+)\b(?=\s+)";
                foreach (Match match in Regex.Matches(outputTool, matchPattern))
                {
                    Process.GetProcessById(int.Parse(match.Value)).Kill();
                }
            }
        }

        #region<<字段>>

        /// <summary>
        /// <para>值：new object()</para>
        /// <para>描述：防止 添加log记录出现死锁现象</para>
        /// </summary>
        private static readonly object Streamlock = new object();

        #endregion
    }
}
