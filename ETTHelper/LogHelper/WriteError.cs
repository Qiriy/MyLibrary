﻿using System;

namespace ETT.Library.ETTHelper.LogHelper
{
    /// <summary>
    /// 记录文本错误信息 WriteError类
    /// </summary>
    public static class WriteError
    {
        /// <summary>
        /// 添加错误日志
        /// </summary>
        /// <param name="msg">错误内容</param>
        /// <param name="count">报错次数 索引从0开始</param>
        public static void WriteErrorLog(string msg, int count = 0)
        {
            lock (Lockobject)
            {

                var errorname = NormalString.ERRORFILENAME;
                try
                {
                    FolderManage.IsFolderExist(PathString.ErrorFilePath, FileEnum.Create);
                    FileManage.IsExistFile(PathString.ErrorFilePath + string.Format(errorname, count), FileEnum.Create);
                    FileManage.AppendLog(PathString.ErrorFilePath + string.Format(errorname, count), msg);
                }
                catch (Exception e)
                {
                    if (count < 4)
                    {
                        count += 1;
                        WriteErrorLog(string.Format(WRITELOGERROR, e.Message, msg), count);
                    }
                }
            }
        }

        #region<<字段>>

        /// <summary>
        /// <para>值：记录错误日志过程中出现错误！ \r\n 内容为 : {0}，原记录内容应为 {1}</para>
        /// <para>描述：记录错误日志时发生错误，再次记录新的错误时的字符串内容</para>
        /// </summary>
        private static readonly string WRITELOGERROR = @"记录错误日志过程中出现错误！ \r\n 内容为 : {0} ，原记录内容应为 {1}";

        /// <summary>
        /// 线程锁
        /// </summary>
        private static readonly object Lockobject = new object();
        #endregion

    }
}
