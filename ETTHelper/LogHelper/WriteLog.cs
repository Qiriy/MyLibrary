﻿using System;

namespace ETT.Library.ETTHelper.LogHelper
{
    /// <summary>
    /// 记录系统日志类
    /// </summary>
    public class WriteLog
    {
        /// <summary>
        /// 写入日志
        /// </summary>
        public static void WriteSystemLog(string msg)
        {
            lock (Lockobject)
            {
            var logname = NormalString.LOGFILENAME;
            int type = 0;
            try
            {
                FolderManage.IsFolderExist(PathString.LogFilePath, FileEnum.Create);
                FileManage.IsExistFile(PathString.LogFilePath + string.Format(logname, type), FileEnum.Create);
                FileManage.AppendLog(PathString.LogFilePath + string.Format(logname, type), msg);
            }
            catch (Exception e)
            {
                WriteError.WriteErrorLog(string.Format(WRITELOGERROR, e.Message, msg));
            }
            }

        }

        #region<<字段>>

        /// <summary>
        /// <para>值：记录日志过程中出现错误！ \r\n 内容为 : {0}，日志记录内容应为 {1}</para>
        /// <para>描述：记录日志时发生错误，再次记录新的错误日志时的字符串内容</para>
        /// </summary>
        private static readonly string WRITELOGERROR = @"记录日志过程中出现错误！ \r\n 内容为 : {0}，日志记录内容应为 {1}";

        /// <summary>
        /// 线程锁
        /// </summary>
        private static readonly object Lockobject = new object();

        #endregion
    }
}
