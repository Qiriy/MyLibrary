﻿using System;
using System.Drawing;

namespace ETT.Library.ETTHelper.CImage
{
    /// <summary>
    /// 图片和文字验证码类
    /// </summary>
    public static class VerificationCode
    {

        /// <summary>
        /// 生成随机的验证码
        /// </summary>
        /// <param name="lenght">[选填] 不填写时默认为四位</param>
        /// <returns>string</returns>
        public static string GenerateCheckCode(int lenght = 0)
        {
            KEYCODE_LENGTH = lenght != 0 ? lenght : 4;
            //定义实验码长度
            string randomCode = string.Empty;
            Random r = new Random();
            for (int i = 0; i < KEYCODE_LENGTH; i++)
            {
                var number = r.Next();
                //字符从0～9，A~Z中随机产生，对应的ASCII码分别为48～57，65～90
                number = number % 36;
                if (number < 10)
                {
                    number += 48;
                }
                else
                {
                    number += 55;
                }
                randomCode += ((char)number).ToString();
            }
            //在Cookie中保存验证码
            return randomCode;
        }

        #region 验证码长度(默认4个验证码的长度)

        private static readonly int length = 4;

        #endregion

        #region 验证码字体大小(为了显示扭曲效果，默认40像素，可以自行修改)

        private static int fontSize = 40;

        #endregion

        #region 边框补(默认1像素)

        private static int padding = 2;

        #endregion

        #region 是否输出燥点(默认不输出)

        private static bool chaos = true;

        #endregion

        #region 输出燥点的颜色(默认灰色)

        private static readonly Color ChaosColor = Color.Coral;

        #endregion

        #region<<字体样式如：黑体等)>>

        static FontStyle[] FontStylesArray =
        {
             FontStyle.Italic, FontStyle.Regular, FontStyle.Strikeout, FontStyle.Bold
        };

        #endregion

        #region 自定义背景色(Chocolate)
        static readonly Color BackgroundColor = Color.White;

        #endregion

        #region 自定义随机颜色数组
        static readonly Color[] Colors = { Color.Black, Color.Red, Color.DarkBlue, Color.Green, Color.Orange, Color.Brown, Color.DarkCyan, Color.Purple };
        #endregion

        #region 自定义字体数组
        static readonly string[] Fonts = { "Arial", "Georgia", "tahoma" };
        #endregion

        #region 产生波形滤镜效果

        private const double Pi2 = 6.283185307179586476925286766559;

        /// <summary>
        /// 正弦曲线Wave扭曲图片（Edit By 51aspx.com）
        /// </summary>
        /// <param name="srcBmp">图片路径</param>
        /// <param name="bXDir">如果扭曲则选择为True</param>
        /// <param name="dMultValue">波形的幅度倍数，越大扭曲的程度越高，一般为3</param>
        /// <param name="dPhase">波形的起始相位，取值区间[0-2*PI)</param>
        /// <returns></returns>
        private static System.Drawing.Bitmap TwistImage(Bitmap srcBmp, bool bXDir, double dMultValue, double dPhase)
        {
            System.Drawing.Bitmap destBmp = new Bitmap(srcBmp.Width, srcBmp.Height);

            // 将位图背景填充为白色
            System.Drawing.Graphics graph = System.Drawing.Graphics.FromImage(destBmp);
            graph.FillRectangle(new SolidBrush(System.Drawing.Color.White), 0, 0, destBmp.Width, destBmp.Height);
            graph.Dispose();

            double dBaseAxisLen = bXDir ? (double)destBmp.Height : (double)destBmp.Width;

            for (int i = 0; i < destBmp.Width; i++)
            {
                for (int j = 0; j < destBmp.Height; j++)
                {
                    double dx = 0;
                    dx = bXDir ? (Pi2 * (double)j) / dBaseAxisLen : (Pi2 * (double)i) / dBaseAxisLen;
                    dx += dPhase;
                    double dy = Math.Sin(dx);

                    // 取得当前点的颜色
                    int nOldX = 0, nOldY = 0;
                    nOldX = bXDir ? i + (int)(dy * dMultValue) : i;
                    nOldY = bXDir ? j : j + (int)(dy * dMultValue);

                    System.Drawing.Color color = srcBmp.GetPixel(i, j);
                    if (nOldX >= 0 && nOldX < destBmp.Width
                     && nOldY >= 0 && nOldY < destBmp.Height)
                    {
                        destBmp.SetPixel(nOldX, nOldY, color);
                    }
                }
            }
            return destBmp;
        }
        #endregion

        #region 生成校验码图片

        /// <summary>
        /// 生成校验码图片
        /// </summary>
        /// <param name="code">校验码文字</param>
        /// <param name="iwidth"></param>
        /// <param name="iheight"></param>
        /// <returns></returns>
        public static byte[] CreateImageCode(string code, int iwidth = 0, int iheight = 0)
        {
            int fSize = fontSize;
            int fWidth = fSize + padding;
            var linecount = length * 4;
            var pointcount = length * 20;
            var zx = 8;
            var zy = 4;
            if (iwidth != 0 || iheight != 0)
            {
                linecount = new Random().Next(1,3) * 2;
                pointcount = (int)(iheight / 5 * 2);
                zx = 3;
                zy = 3;
            }
            if (iwidth == 0)
            {
                iwidth = (int)(code.Length * fWidth) + 30 + padding * 2;
            }
            if (iheight == 0)
            {
                iheight = fSize * 2 + padding;
            }
            else
            {
                fontSize = fSize = iheight * 9 / 10 - padding - 6;
                fWidth = fSize + padding;
            }
            System.Drawing.Bitmap image = new System.Drawing.Bitmap(iwidth, iheight);
            Graphics g = Graphics.FromImage(image);
            g.Clear(BackgroundColor);
            Random rand = new Random();
            //给背景添加随机生成的燥点
            if (chaos)
            {
                //绘制噪线
                for (int i = 0; i < linecount; i++)
                {
                    int x1 = rand.Next(image.Width);
                    int x2 = rand.Next(image.Width);
                    int y1 = rand.Next(image.Height);
                    int y2 = rand.Next(image.Height);
                    g.DrawLine(new Pen(Colors[rand.Next(Colors.Length - 1)]), x1, y1, x2, y2);
                }
                //绘制噪点
                Pen pen = new Pen(ChaosColor, 0);
                int c = pointcount;
                for (int i = 0; i < c; i++)
                {
                    int x = rand.Next(image.Width);
                    int y = rand.Next(image.Height);
                    g.DrawRectangle(pen, x, y, rand.Next(5), rand.Next(5));
                }
            }
            int top1 = 1, top2 = 1;
            int n1 = (iheight - fontSize - padding * 2);
            int n2 = n1 / 4;
            top1 = n2;
            top2 = n2 * 2;
            //随机字体和颜色的验证码字符
            for (int i = 0; i < code.Length; i++)
            {
                var cindex = rand.Next(Colors.Length - 1);
                var findex = rand.Next(Fonts.Length - 1);
                var fontindex = rand.Next(FontStylesArray.Length - 1);
                var f = new System.Drawing.Font(Fonts[findex], fSize, (FontStylesArray[fontindex]));
                Brush b = new System.Drawing.SolidBrush(Colors[cindex]);
                var top = 0;
                top = i % 2 == 1 ? top2 : top1;
                var left = i * (fWidth - 1);
                g.DrawString(code.Substring(i, 1), f, b, left, top);
            }
            //画一个边框 边框颜色为Color.Gainsboro
            //产生波形（Add By 51aspx.com）
            image = TwistImage(image, true, zx, zy);
            g.DrawRectangle(new Pen(Color.LightGray, 0), 0, 0, image.Width - 1, image.Height - 1);
            g.Dispose();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                //图片格式制定为PNG
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
        }

        #endregion


        #region <<常量>>
        /// <summary>
        /// <para>值：4</para>
        /// <para>描述：验证码长度</para>
        /// </summary>
        private static int KEYCODE_LENGTH = 4;

        #endregion
    }
}
