﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ETT.Library.ETTHelper.LogHelper;
using Microsoft.International.Converters.PinYinConverter;

//命名空间
namespace ETT.Library.ETTHelper.StringLibaray
{
    /// <summary>
    /// 字符串帮助类
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// 转化为Integer
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int ToInteger(this object str)
        {
            if (str == DBNull.Value || str == null || str == string.Empty)
            {
                return 0;
            }
            else
            {
                return SafeConvert.ToInt32(str);
            }
        }

        /// <summary>
        /// 转化为Integer
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int ToTryInteger(this object str)
        {
            int result = 0;
            int.TryParse((string)str, out result);
            return result;
        }

        /// <summary>
        /// 转化为Long
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static long ToLong(this object str)
        {
            if (str == DBNull.Value || str == null || str == string.Empty)
            {
                return 0;
            }
            else
            {
                return SafeConvert.ToInt64(str);
            }
        }

        /// <summary>
        /// 转化为Double
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static double ToDouble(this object str)
        {
            if (str == DBNull.Value || str == null || str == string.Empty)
            {
                return 0;
            }
            else
            {
                return SafeConvert.ToDouble(str);
            }
        }

        /// <summary>
        /// 格式化空字符串
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string FormatEmptyStr(this object str)
        {
            if (str == DBNull.Value || str == null || str == string.Empty)
            {
                return "--";
            }
            else
            {
                return str.ToString();
            }
        }

        /// <summary>
        /// 格式化空字符串
        /// </summary>
        /// <param name="str"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GetSubString(this object str, int length)
        {
            if (str == DBNull.Value || str == null)
            {
                return string.Empty;
            }
            else
            {
                var subString = string.Empty;
                if (subString.Length <= length)
                {
                    subString = str.ToString();
                }
                else
                {
                    subString = str.ToString().Substring(0, length) + "...";
                }
                return subString;
            }
        }

        /// <summary>
        /// 格式化空字符串
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToShortDateString(this object str)
        {
            string result;
            if (str == DBNull.Value || str == null || str.ToString() == string.Empty)
            {
                result = string.Empty;
            }
            else
            {
                try
                {
                    DateTime date = DateTime.Parse(str.ToString());
                    result = date.ToShortDateString();
                }
                catch (Exception ex)
                {
                    WriteError.WriteErrorLog(ex.Message);
                    result = string.Empty;
                }
            }
            return result;
        }

        /// <summary>
        /// 处理异常数据转换
        /// </summary>
        /// <param name="str"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int SafeToInt32(string str, int value = 0)
        {
            int result;
            try
            {
                result = SafeConvert.ToInt32(value != 0 ? str.Substring(0, value) : str);
            }
            catch
            {
                result = 0;
            }
            return result;
        }

        /// <summary>
        /// 检验时间字符串正确性
        /// </summary>
        /// <param name="timestring"></param>
        /// <returns></returns>
        public static bool CheckTime(string timestring)
        {
            bool bReslut = true;
            try
            {
                Regex reg = new Regex(@"^([1-2]\d{3})[\/|\-](0?[1-9]|10|11|12)[\/|\-]([1-2]?[0-9]|0[1-9]|30|31)$");
                if (reg.IsMatch(timestring))
                {
                    Convert.ToDateTime(timestring);
                }
                else
                {
                    bReslut = false;
                }
            }
            catch
            {
                bReslut = false;
            }
            return bReslut;
        }

        #region <<数据长度截取>>

        /// <summary>  
        /// 对字符串进行裁剪  
        /// </summary>  
        public static string Trim(string stringTrim, int maxLength)
        {
            return Trim(stringTrim, maxLength, "...");
        }

        /// <summary>  
        /// 对字符串进行裁剪(区分单字节及双字节字符)  
        /// </summary>  
        /// <param name="rawString">需要裁剪的字符串</param>  
        /// <param name="maxLength">裁剪的长度，按双字节计数</param>  
        /// <param name="appendString">如果进行了裁剪需要附加的字符</param>  
        public static string Trim(string rawString, int maxLength, string appendString)
        {
            if (string.IsNullOrEmpty(rawString) || rawString.Length <= maxLength)
            {
                return rawString;
            }
            else
            {
                int rawStringLength = Encoding.UTF8.GetBytes(rawString).Length;
                if (rawStringLength <= maxLength * 2)
                    return rawString;
            }

            int appendStringLength = Encoding.UTF8.GetBytes(appendString).Length;
            StringBuilder checkedStringBuilder = new StringBuilder();
            int appendedLenth = 0;
            for (int i = 0; i < rawString.Length; i++)
            {
                char _char = rawString[i];
                checkedStringBuilder.Append(_char);

                appendedLenth += Encoding.Default.GetBytes(new char[] { _char }).Length;

                if (appendedLenth >= maxLength * 2 - appendStringLength)
                    break;
            }

            return checkedStringBuilder.ToString() + appendString;
        }


        #endregion

        #region <<字符串验证>>

        /// <summary>
        /// 是否为数字
        /// </summary>
        /// <param name="str"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static bool IsNum(string str, int len = 0)
        {
            bool bResult = false;
            if (len == 0)
            {
                bResult = Regex.IsMatch(str, "^[0-9]+$");
            }
            else
            {
                bResult = Regex.IsMatch(str, "^[0-9]{" + len + "}$");
            }
            return bResult;
        }

        /// <summary>
        /// 验证数字和字母和下划线
        /// </summary>
        /// <param name="str"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static bool IsNumOrAlpOrLine(string str, int len = 0)
        {
            bool bResult = false;
            bResult = len == 0 ? Regex.IsMatch(str, @"^[a-zA-Z0-9_-]+$") : Regex.IsMatch(str, @"^[a-zA-Z0-9_-]{" + len + "}$");
            return bResult;
        }

        /// <summary>
        /// 验证数字和字母
        /// </summary>
        /// <param name="str"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static bool IsNumOrAlp(string str, int len = 0)
        {
            bool bResult = false;
            bResult = len == 0 ? Regex.IsMatch(str, @"^[a-zA-Z0-9]+$") : Regex.IsMatch(str, @"^[a-zA-Z0-9]{" + len + "}$");
            return bResult;
        }

        /// <summary>
        /// 验证非汉字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsChinese(string str)
        {
            return !Regex.IsMatch(str, @"^[\u4e00-\u9fa5]+$");
        }
        #endregion

        #region<<获取全品简拼>>

        /// <summary>
        /// 从 <see cref="ChineseChar"/>获取没有读音格式的拼音，包含多音，并去掉重复的<para />
        /// 结果格式：
        ///  "重" 将得到 "ZHONG" 和 "CHONG", 
        ///  "量" 只会返回 "LIANG".
        /// </summary>
        /// <param name="chineseChar"></param>
        /// <returns></returns>
        private static IEnumerable<string> PinyinsWithoutSound(this ChineseChar chineseChar)
        {
            return chineseChar.Pinyins.Where(p => p != null).Select(p => p.Substring(0, p.Length - 1)).Distinct();
        }

        /// <summary>
        /// 获取汉字拼音
        /// 此封装不会返读声，即最后面的1,2,3,4,5(平)声
        /// </summary>
        /// <param name="chinese">简体中文汉字(实际上大量繁体字也支持)</param>
        /// <returns>汉字拼音</returns>
        public static string GetJianPin(this string chinese)
        {
            Char[] chineseChars = chinese.ToCharArray();
            IEnumerable<string> pinYins = chineseChars.GetPinyins(0);
            string r = pinYins.Aggregate(string.Empty, (current, py) => current + ("" + py + ","));
            r = r.Substring(0, r.Length - 1);
            return r;
        }

        /// <summary>
        /// 从指定位置把有汉字的拼音拼接起来返回，并用递归方式组合了多音字的发音
        /// </summary>
        /// <param name="chinese">汉字串</param>
        /// <param name="startIndex">起始位置</param>
        /// <returns></returns>
        private static IEnumerable<string> GetPinyins(this char[] chinese, int startIndex)
        {
            //如果startIndex等于字符长度，直接返回空
            if (startIndex == chinese.Length)
            {
                yield return string.Empty;
            }
            else
            {
                //如果该字符是中文则取出读音返回
                if (ChineseChar.IsValidChar(chinese[startIndex]))
                {
                    foreach (var charPinyin in (new ChineseChar(chinese[startIndex])).PinyinsWithoutSound())
                    {
                        foreach (var remainingPinyins in chinese.GetPinyins(startIndex + 1))
                        {
                            //只取首字母，把substring去掉就是全拼
                            yield return (charPinyin.Substring(0, 1) + remainingPinyins);
                        }
                    }
                }
                else
                {
                    //如果不是中文则直接返回字符本身，这样就解决中英数混合串
                    foreach (var remainingPinyins in chinese.GetPinyins(startIndex + 1))
                    {
                        yield return chinese[startIndex] + remainingPinyins;
                    }
                }
            }
        }

        /// <summary>
        ///  从<see cref="ChineseChar"/>获取没有读音格式的拼音，包含多音，并去掉重复的<para />
        /// 结果格式：
        ///  "重" 将得到 "ZHONG" 和 "CHONG", 
        ///  "量" 只会返回 "LIANG".
        /// </summary>
        /// <param name="chinese"></param>
        /// <returns></returns>
        public static string GetQuanPin(this string chinese)
        {
            string r = string.Empty;
            foreach (char obj in chinese)
            {
                try
                {
                    ChineseChar chineseChar = new ChineseChar(obj);
                    string t = chineseChar.Pinyins[0].ToString();
                    r += t.Substring(0, t.Length - 1);
                }
                catch
                {
                    r += obj.ToString();
                }
            }
            return r;
        }
        #endregion
    }
}
