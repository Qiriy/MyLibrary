﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETT.Library.ETTHelper.StringLibaray
{
    public class CBase64Helper
    {
        //原始的字节数组
        private byte[] orgin = null;

        //以3个字母分组后的字节数组
        private byte[] three = null;

        //以4个字母分组
        private byte[] four = null;

        //base64基本字符表
        private const string table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

        //以4个分组的常量
        private byte[] breaks = new byte[6]{
                                  0xFC,//1111 1100
                                  0x3, //0000 0011
                                  0xF0,//1111 0000
                                  0xF, //0000 1111
                                  0xC0,//1100 0000
                                  0x3F,//0011 1111
        };
        /// <summary>
        /// 
        /// </summary>
        public CBase64Helper()
        {
        }

        /// <summary>
        /// 对字节编码
        /// </summary>
        /// <param name="input">输入的字节</param>
        /// <returns>string</returns>
        public string Encoder(byte[] input)
        {
            string retstr = string.Empty;
            if (input.Length >= 0)
            {
                int block = 0;

                orgin = input;

                //以三个字母分组，不够的0补齐;
                block = orgin.Length % 3 > 0 ? (orgin.Length / 3) + 1 : orgin.Length / 3;

                //创建三个字符组成的数组字节
                three = new byte[block * 3];
                four = new byte[block * 4];
                for (int i = 0; i < block * 3; i++)
                {
                    if (i < orgin.Length)
                    {
                        three[i] = orgin[i];
                    }
                    else
                    {
                        three[i] = (byte)0;
                    }
                }
                //以4个分组
                byte a = 0, b = 0, c = 0;
                for (int i = 0; i < block; i++)
                {
                    a = three[i * 3];
                    b = three[i * 3 + 1];
                    c = three[i * 3 + 2];

                    //组成第一个字节，取第一个数，向右移2位
                    four[i * 4] = (byte)(a >> 2);
                    //组成第二个字节，取第一个数的后两位,向左移动4位,
                    //取第二个字节，向右移动4位
                    four[i * 4 + 1] = (byte)(((a & breaks[1]) << 4) | (b >> 4));
                    //取第二个数的后四位并向前移动两个
                    four[i * 4 + 2] = (byte)(((b & breaks[3]) << 2) | c >> 6);
                    //取第三个数,先向左移动两位，再向右移动两位
                    four[i * 4 + 3] = (byte)(c & breaks[5]);
                }
                for (int i = 0; i < four.Length; i++)
                {
                    retstr += four[i] >= 0 && four[i] <= 63 ? table[four[i]].ToString() : "";
                }

                switch (input.Length % 3)
                {
                    case 1:
                        retstr = retstr.Substring(0, retstr.Length - 2) + "==";
                        break;
                    case 2:
                        retstr = retstr.Substring(0, retstr.Length - 1) + "=";
                        break;
                }

                return retstr;

            }
            else
            {
                return string.Empty;
            }

        }
        /// <summary>
        /// base64编码
        /// </summary>
        /// <param name="str">要编码的字符串</param>
        /// <returns>已编码的字符串</returns>
        public string Encoder(string str)
        {
            return Encoder(str, (System.Text.Encoding)null);
        }
        /// <summary>
        /// base64编码
        /// </summary>
        /// <param name="str">要编码的字符串</param>
        /// <param name="eg">编码格式</param>
        /// <returns>已编码的字符串</returns>
        public string Encoder(string str, System.Text.Encoding eg)
        {
            if (string.Empty != str)
            {
                if (eg == null)
                {
                    eg = System.Text.Encoding.Default;
                }
                return Encoder(eg.GetBytes(str));
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// base64编码文件
        /// </summary>
        /// <param name="filename">要编码的文件</param>
        /// <param name="tofile">编码后的文件</param>
        /// <returns>是否编码成功</returns>
        public bool Encoder(string filename, string tofile)
        {
            return Encoder(filename, tofile, null);
        }
        /// <summary>
        /// base64编码文件
        /// </summary>
        /// <param name="filename">要编码的文件</param>
        /// <param name="tofile">编码后的文件</param>
        /// <param name="eg" >编码格式</param>
        /// <returns>是否编码成功</returns>
        public bool Encoder(string filename, string tofile, System.Text.Encoding eg)
        {
            if (filename != string.Empty)
            {
                if (System.IO.File.Exists(filename))
                {
                    if (tofile != string.Empty)
                    {
                        using (System.IO.FileStream fs = System.IO.File.Create(tofile))
                        {
                            System.IO.FileStream from = new System.IO.FileStream(filename,
                                System.IO.FileMode.Open,
                                System.IO.FileAccess.Read);
                            byte[] input = new byte[from.Length];
                            from.Read(input, 0, (int)from.Length);
                            string stemp = Encoder(input);
                            if (eg == null)
                            {
                                eg = System.Text.Encoding.Default;
                            }
                            byte[] stem = eg.GetBytes(stemp);
                            fs.Write(stem, 0, stem.Length);
                            fs.Flush();
                            fs.Close();
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public byte[] Decoder(byte[] input)
        {
            if (input.Length > 0)
            {
                int block = 0;
                four = input;
                block = four.Length / 4;
                three = new byte[block * 3];
                byte a = 0, b = 0, c = 0, d = 0;
                for (int i = 0; i < block; i++)
                {
                    a = four[i * 4];
                    b = four[i * 4 + 1];
                    c = four[i * 4 + 2];
                    d = four[i * 4 + 3];
                    //第一个数，向左移2位，第二个数，向右移4位，然后或于第二个数
                    three[i * 3] = (byte)((a << 2) | (b >> 4));
                    //第二个数向左移动4位，每三个数向左移动2位，最后或于
                    three[i * 3 + 1] = (byte)((b << 4) | (c >> 2));
                    //第三个数据向左移动6位，然后或于第4个数
                    three[i * 3 + 2] = (byte)((c << 6) | d);
                }
                return three;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// base64解码
        /// </summary>
        /// <param name="str">将要解码的字符串</param>
        /// <returns>解码的字符串</returns>
        public string Decoder(string str)
        {
            if (str != string.Empty)
            {
                string retstr = string.Empty;
                retstr = Decoder(str, (System.Text.Encoding)null);

                return retstr;
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// base64解码
        /// </summary>
        /// <param name="str">将要解码的字符串</param>
        /// <param name="eg">编码格式</param>
        /// <returns>解码的字符串</returns>
        public string Decoder(string str, System.Text.Encoding eg)
        {
            if (str != string.Empty)
            {
                //如果除以4不为零，表示不是标准的base64加密过的字符串，将字符串截取后再解码

                string retstr = string.Empty;
                if (str.Length % 4 != 0)
                {
                    str = str.Substring(0, (str.Length / 4) * 4);
                }

                int t = 0;
                for (int i = 0; i < str.Length; i++)
                {
                    t = table.IndexOf(str[i]);
                    four[i] = ((t != -1) ? (byte)t : (byte)0);
                }
                three = Decoder(four);

                if (eg == null)
                {
                    eg = System.Text.Encoding.Default;
                }
                retstr = eg.GetString(three);
                retstr = retstr.Trim('\0');
                return retstr;
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// base64解码文件
        /// </summary>
        /// <param name="fromfile">输入文件</param>
        /// <param name="tofile">输出文件</param>
        /// <returns>bool</returns>
        public bool Decoder(string fromfile, string tofile)
        {
            return Decoder(fromfile, tofile, null);
        }
        /// <summary>
        /// base64解码文件
        /// </summary>
        /// <param name="fromfile">输入文件</param>
        /// <param name="tofile">输出文件</param>
        /// <param name="eg">解码格式</param>
        /// <returns>bool</returns>
        public bool Decoder(string fromfile, string tofile, System.Text.Encoding eg)
        {
            if (fromfile != string.Empty)
            {
                if (System.IO.File.Exists(fromfile))
                {
                    if (tofile != string.Empty)
                    {
                        using (System.IO.FileStream fs = System.IO.File.Create(tofile))
                        {
                            System.IO.FileStream from = new System.IO.FileStream(fromfile,
                                System.IO.FileMode.Open,
                                System.IO.FileAccess.Read);
                            byte[] input = new byte[from.Length];
                            from.Read(input, 0, (int)from.Length);
                            if (eg == null)
                            {
                                eg = System.Text.Encoding.Default;
                            }
                            string str = eg.GetString(input);
                            if (str.Length % 4 != 0)
                            {
                                str = str.Substring(0, (str.Length / 4) * 4);
                            }

                            int t = 0;
                            for (int i = 0; i < str.Length; i++)
                            {
                                t = table.IndexOf(str[i]);
                                four[i] = ((t != -1) ? (byte)t : (byte)0);
                            }
                            three = Decoder(four);
                            fs.Write(three, 0, three.Length);
                            fs.Flush();
                            fs.Close();
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
