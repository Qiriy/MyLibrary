﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace ETT.Library.ETTHelper.Computer
{
    /// <summary>
    /// 注册表管理类
    /// </summary>
    public class RegistryKeyInfo
    {
        private static string _itemname = string.Empty;

        /// <summary>  
        /// 注册表左侧的项（左半边带有文件夹图标的节点）  
        /// </summary>  
        public static string ItemName
        {
            set
            {
                _itemname = value;
            }
            get
            {
                return _itemname;
            }
        }

        /// <summary>  
        /// 增加键，如果没有，创建  
        /// </summary>  
        /// <param name="keyName">键的名称</param>  
        /// <param name="keyValue">键的数值</param>  
        public static void RegeditAdd(string keyName, string keyValue)
        {
            //首先判断是否存在ItemName项，没有就建立  
            if (!RegeditItemExist(ItemName))//  
            {
                //获取HKEY_LOCAL_MACHINE节点  
                RegistryKey key = Registry.LocalMachine;

                //建立该节点，如果该节点已经存在，则不影响  
                key.CreateSubKey(REGISTRYKEYVALUE_PATH + ItemName);
            }
            if (!RegeditKeyExist(keyName))//然后判断Zane项中是否有zane键，没有就添加默认密码记录  
            {
                RegistryKey key = Registry.LocalMachine;
                RegistryKey software = key.OpenSubKey(REGISTRYKEYVALUE_PATH + ItemName, true);  //该项必须已存在  

                if (software != null) software.SetValue(keyName, keyValue);
            }
        }

        /// <summary>  
        /// 根据节点的名称，验证该节点是否存在   
        /// </summary>  
        /// <returns></returns>  
        public static bool RegeditItemExist(string itemName)
        {
            RegistryKey hkml = Registry.LocalMachine;
            RegistryKey software = hkml.OpenSubKey(REGISTRYKEYVALUE);

            //如果注册表中不存在该项则返回  
            if (software == null)
                return false;

            var subkeyNames = software.GetSubKeyNames();
            //取得该项下所有子项的名称的序列，并传递给预定的数组中    
            foreach (string keyName in subkeyNames)   //遍历整个数组    
            {
                //判断子项的名称    
                if (keyName == itemName)
                {
                    hkml.Close();
                    return true;
                }
            }
            hkml.Close();
            return false;
        }

        /// <summary>  
        /// 根据键的名称，验证该键是否存在  
        /// </summary>  
        /// <param name="keyName">键的名称</param>  
        /// <returns></returns>  
        public static bool RegeditKeyExist(string keyName)
        {
            RegistryKey hkml = Registry.LocalMachine;
            RegistryKey software = hkml.OpenSubKey(REGISTRYKEYVALUE_PATH + ItemName);
            //如果注册表中不存在该项则返回  
            if (software == null)
                return false;
            var subkeyNames = software.GetValueNames();
            //取得该项下所有键值的名称的序列，并传递给预定的数组中     
            if (subkeyNames.Any(itemKeyName => itemKeyName == keyName))
            {
                hkml.Close();
                return true;
            }
            hkml.Close();
            return false;
        }

        /// <summary>  
        /// 根据键的名称，获得该键的数据  
        /// </summary>  
        /// <param name="keyName">键的名称</param>  
        /// <returns></returns>  
        public static string GetValeByName(string keyName)
        {
            string regName = "";
            RegistryKey key = Registry.LocalMachine;
            RegistryKey zane = key.OpenSubKey(REGISTRYKEYVALUE_PATH + ItemName, true);
            if (zane != null)
            {
                regName = zane.GetValue(keyName).ToString();
                zane.Close();
            }
            return regName;
        }

        /// <summary>  
        /// 根据键的名称，删除该键（值）  
        /// </summary>  
        /// <param name="keyName">键的名称</param>  
        /// <returns></returns>  
        public static bool DeleteRegist(string keyName)
        {
            RegistryKey hkml = Registry.LocalMachine;
            RegistryKey software = hkml.OpenSubKey(REGISTRYKEYVALUE, true);
            if (software != null)
            {
                RegistryKey item = software.OpenSubKey(ItemName, true);
                //如果注册表中不存在该项则返回  
                if (item == null)
                    return false;

                var keys = item.GetValueNames();
                foreach (string key in keys)
                {
                    if (key == keyName)
                    {
                        item.DeleteValue(key);
                        return true;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// <para>值：SOFTWARE</para>
        /// <para>描述：</para>
        /// </summary>
        private static string REGISTRYKEYVALUE = @"SOFTWARE";

        /// <summary>
        /// <para>值：SOFTWARE//</para>
        /// <para>描述：</para>
        /// </summary>
        private static string REGISTRYKEYVALUE_PATH = @"SOFTWARE//";
    }
}
