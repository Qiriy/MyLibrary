﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.ETTHelper.Factory
{
    /// <summary>
    /// 反射实现类
    /// </summary>
    public class CCreate
    {

        /// <summary>
        /// 根据 dll创建新的实例
        /// </summary>
        /// <param name="classpath">dll绝对路径</param>
        /// <param name="allname">类的全名</param>
        /// <returns></returns>
        public static object CreateNewOjb(string classpath, string allname)
        {
            object obj = null;
            try
            {
                if (!File.Exists(classpath))
                {
                    throw new System.Exception("指定位置的程序集不存在，不能实例化！");
                }
                else
                {
                    Assembly assembly = Assembly.LoadFile(classpath); // 加载程序集（EXE 或 DLL） 
                    obj = assembly.CreateInstance(allname); // 创建类的实例
                }
            }
            catch (Exception e)
            {
                LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            return obj;
        }

    }
}
