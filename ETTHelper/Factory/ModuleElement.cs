﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ETT.Library.ETTHelper.Factory
{
    /// <summary>
    /// factory类解析config-module节点
    /// </summary>
    public class ModuleElement : ConfigurationElement
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public ModuleElement()
        {
        }

        /// <summary>
        /// 节点名
        /// </summary>
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return this["name"].ToString(); }
        }

        /// <summary>
        /// BLL程序集名称
        /// </summary>
        [ConfigurationProperty("bllassembly", IsRequired = true)]
        public string BllAssemblyName
        {
            get { return this["bllassembly"].ToString(); }
        }

        /// <summary>
        /// BLL程序集命名空间
        /// </summary>
        [ConfigurationProperty("bllnamespace", IsRequired = true)]
        public string BllAssemblySpace
        {
            get { return this["bllnamespace"].ToString(); }
        }

        /// <summary>
        /// DAL程序集名称
        /// </summary>
        [ConfigurationProperty("dalassembly", IsRequired = true)]
        public string DalAssemblyName
        {
            get { return this["dalassembly"].ToString(); }
        }

        /// <summary>
        /// DAL程序集命名空间
        /// </summary>
        [ConfigurationProperty("dalnamespace", IsRequired = true)]
        public string DalAssemblySpace
        {
            get { return this["dalnamespace"].ToString(); }
        }
    }
}
