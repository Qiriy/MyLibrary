﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.ETTHelper.MyException
{

    public class CException : System.Exception
    {
        public CException(string msg)
        {
            _msg = msg;
        }

        public override string Message
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(_msg))
                {
                    return _msg;
                }
                return "错误信息未指定";
            }
        }


        private string _msg = string.Empty;
    }
}
