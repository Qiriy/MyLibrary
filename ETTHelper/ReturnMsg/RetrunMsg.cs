﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.ETTHelper.ReturnMsg
{
    /// <summary>
    /// 消息返回类
    /// </summary>
    [Serializable]
    public class RetrunMsg
    {
        /// <summary>
        /// 消息提示
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public object Content { get; set; }

        /// <summary>
        /// 内容长度
        /// </summary>
        public int Length { get; set; }
    }
}
