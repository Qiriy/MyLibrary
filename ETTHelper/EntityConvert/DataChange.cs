﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.ETTHelper.EntityConvert
{
    /// <summary>
    /// 二进制数据类型转换
    /// </summary>
    public class DataChange
    {

        //MemoryStream: 创建其支持存储区为内存的流。
        //IFormatter : 提供将序列化对象格式化的功能。 
        //DataSet转二进制byte
        /// <summary>
        /// dataset 转二进制数据
        /// </summary>
        /// <param name="dsOriginal"></param>
        /// <returns></returns>
        public static byte[] GetBinaryFormatData(DataSet dsOriginal)
        {
            byte[] binaryDataResult = null;
            MemoryStream memStream = new MemoryStream();
            //以二进制格式将对象或整个连接对象图形序列化和反序列化。
            IFormatter brFormatter = new BinaryFormatter();
            //dsOriginal.RemotingFormat 为远程处理期间使用的DataSet 获取或设置 SerializtionFormat        
            //SerializationFormat.Binary      将字符串比较方法设置为使用严格的二进制排序顺序
            dsOriginal.RemotingFormat = SerializationFormat.Binary;
            //把字符串以二进制放进memStream中
            brFormatter.Serialize(memStream, dsOriginal);
            //转为byte数组
            binaryDataResult = memStream.ToArray();
            memStream.Close();
            memStream.Dispose();
            return binaryDataResult;
        }
        //二进制byte转DataSet
        /// <summary>
        /// 将字节数组反序列化成DataSet对象
        /// </summary>
        /// <param name="binaryData">字节数组</param>
        /// <returns>DataSet对象</returns>
        public static DataSet RetrieveDataSet(byte[] binaryData)
        {
            DataSet ds = null;
            MemoryStream memStream = new MemoryStream(binaryData, true);
            IFormatter brFormatter = new BinaryFormatter();
            ds = (DataSet)brFormatter.Deserialize(memStream);
            return ds;
        }

        /// <summary>
        /// 将object格式化成字节数组byte[]
        /// </summary>
        /// <param name="dsOriginal">object对象</param>
        /// <returns>字节数组</returns>
        public static byte[] GetBinaryFormatData(object dsOriginal)
        {
            byte[] binaryDataResult = null;
            MemoryStream memStream = new MemoryStream();
            IFormatter brFormatter = new BinaryFormatter();
            brFormatter.Serialize(memStream, dsOriginal);
            binaryDataResult = memStream.ToArray();
            memStream.Close();
            memStream.Dispose();
            return binaryDataResult;
        }
        /// <summary>
        /// 将byte[]字节数组反序列化成object对象
        /// </summary>
        /// <param name="binaryData">字节数组</param>
        /// <returns>object对象</returns>
        public static object RetrieveObject(byte[] binaryData)
        {
            MemoryStream memStream = new MemoryStream(binaryData);
            IFormatter brFormatter = new BinaryFormatter();
            Object obj = brFormatter.Deserialize(memStream);
            return obj;
        }
    }
}
