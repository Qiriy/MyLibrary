﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.LogHelper
{
    internal class NormalString
    {
        /// <summary>
        /// <para>值：yyyyMMdd</para>
        /// <para>描述：记录log等时使用的时间格式</para>
        /// </summary>
        internal static string NORMALDATELOGSTRING = "yyyyMMdd";

        /// <summary>
        /// <para>值：\SystemLog\</para>
        /// <para>描述：记录log等时使用的前缀路径</para>
        /// </summary>
        internal static string NORMALDATELOGPREFIX = "\\SystemLog\\";

        /// <summary>
        /// <para>值：\SystemLog\</para>
        /// <para>描述：记录错误信息时使用的前缀路径</para>
        /// </summary>
        internal static string NORMALDATEERRORPREFIX = "\\ErrorLog\\";

        /// <summary>
        /// <para>值：yyyy-MM-dd HH:mm:ss</para>
        /// <para>描述：时间转换字符串格式时使用的时间格式</para>
        /// </summary>
        internal static string NORMALDTAESTRING = "yyyy-MM-dd HH:mm:ss";
    }
}
