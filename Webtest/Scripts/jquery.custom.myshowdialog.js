﻿//;(function ($, window, document, undefined) {
var mydialog;
(function ($) {
    var _options;
    var selectelement;
    var dialogleft = 0;
    var dialogtop = 0;
    var dialogheight = 0;
    var dialogwidth = 0;
    var ddiv = '';
    var durl = '';
    var dhtml = '';
    var dindex = 0;
    var dtitle = '';
    var lbuttonvalue = '';
    var rbuttonvalue = '';
    var startIframe = false;
    var dcenter = false;
    var lbuttonShow = true;
    var rbuttonShow = true;
    var xscroll = false;
    var yscroll = false;
    var bodywidth = 0;//window.document.body.clientWidth;
    var bodyheight = 0;// window.document.body.clientHeight;
    var indexselectarray = [];//存储多个弹出框的 index 及弹出框的的父元素
    var onOkClick = null;
    var onCancelClick = null;

    //定义zDiaLog的构造函数
    var zDiaLog = function (ele, opt) {
        selectelement = ele.selector;
        this.$element = ele,
            this.defaults = {
                'width': '800',
                'height': '400',
                'lbuttonvalue': '确认',
                'rbuttonvalue': '取消',
                'lbuttonShow': true,
                'rbuttonShow':true,
                'startIframe': false,
                'ddiv': '',
                'durl': '',
                'dindex': 0,
                'dtitle': '',
                'xscroll': false,
                'yscroll': false,
                'dcenter': false,
                'left': '200',
                'top': '100',
                'onOkClick': null,
                'onCancelClick': null
            },
        this.options = $.extend({}, this.defaults, opt);
    }
    //定义zDiaLog的方法
    /**
    * 创建参数
    * @param {} dom 
    * @returns {} 
    */
    function gethtml() {
        var chtml =
            '<div class="showdialogbody">' +
                '<div class="showdialog_width">' +
                '<div class="lefttop"></div>' +
                '<div class="Xtopcenter"></div>' +
                '<div class="righttop"></div>' +
                '</div>' +
                '<div class="showdialog_width">' +
                '<div class="Yleftcenter"></div>' +
                '   <div class="centertr">' +
                '       <div class="dtitle">' +
                '           <div class="lefttitle"></div>' +
                '           <div class="centertitle"></div>' +
                '           <div class="righttitle"></div>' +
                '       </div>' +
                '       <div class="closedialog" onclick="mydialog.HiddenDiaLog(' + dindex + ')"></div>' +
                '       <div class="title_value"></div>' +
                '       <div class="center_body">' +
                '        </div>' +
                '       <div class="bottom_buttonarea">' +
                '           <span class="button_cancel" pfather="' + selectelement + '">' + rbuttonvalue + '</span>' +
                '           <span class="button_empty"></span>' +
                '           <span class="button_ok" pfather="' + selectelement + '" >' + lbuttonvalue + '</span>' +
                '       </div>' +
                '   </div>' +
                '   <div class="Yrightcenter"></div>' +
                '</div>' +
                '<div class="showdialog_width">' +
                '   <div class="leftbottom"></div>' +
                '   <div class="Xbottomcenter"></div>' +
                '   <div class="rightbottom"></div>' +
                '</div>' +
                ' </div>';
        return chtml;
    }


    /**
     * 获取当前选中
     * @param {} dom 
     * @returns {} 
     */
    function thisselect(dom) {
        var thiss = 0;
        for (var i = 0; i < indexselectarray.length; i++) {
            var obj = indexselectarray[i];
            if (obj.name == dom) {
                thiss = obj.value;
                break;
            }
        }
        return thiss;
    }

    /**
     * 当前对象
     * @returns {} 
     */
    function thisobject(dom) {
        var thiss = new Object();
        for (var i = 0; i < indexselectarray.length; i++) {
            var obj = indexselectarray[i];
            if (obj.value == dom) {
                thiss = obj;
                break;
            }
        }
        return thiss;
    }



    /**
     * 设置屏幕尺寸等内容
     * @returns {} 
     */
    function setSize() {
        bodywidth = $(window).width();
        bodyheight = $(window).height();
        $(selectelement + ' .showdialogbody').css("z-index", 9999 + dindex);
        $(selectelement).css("display", 'none');
        var leftthis = dialogleft;
        var topthis = dialogtop;
        if (dcenter) {
            leftthis = (bodywidth - dialogwidth - 20) / 2;
            topthis = (bodyheight - dialogheight + 20) / 2;
        }
        $(selectelement + ' .showdialogbody').css("left", leftthis + 'px');
        $(selectelement + ' .showdialogbody').css("top", topthis + 'px');
        $(selectelement + ' .Xtopcenter').css("width", dialogwidth + 'px');
        $(selectelement + ' .Xbottomcenter').css("width", dialogwidth + 'px');
        $(selectelement + ' .Yleftcenter').css("height", dialogheight + 'px');
        $(selectelement + ' .Yrightcenter').css("height", dialogheight + 'px');
        $(selectelement + ' .centertr').css("height", dialogheight + 'px');
        $(selectelement + ' .centertr').css("width", dialogwidth + 'px');
        $(selectelement + ' .dtitle').css("width", (parseInt(dialogwidth) + 10) + 'px');
        $(selectelement + ' .centertitle').css("width", (parseInt(dialogwidth) - 4) + 'px');
        $(selectelement + ' .center_body').css("height", (parseInt(dialogheight) - 110) + 'px');
        $(selectelement + ' .showdialog_width').css("width", (parseInt(dialogwidth) + 20) + 'px');
    }

    /**
     * 设置正文内容
     * @returns {} 
     */
    function setContent() {
        if (durl != '') {
            $(selectelement + ' .center_body').empty();
            if (startIframe) {
                $(selectelement + ' .center_body').html('<iframe src="' + durl + '" width="100%" height="' + (parseInt(dialogheight) - 117) + '"> </iframe> ');
            } else {
                $(selectelement + ' .center_body').load(durl, function (response, status, xhr) {
                    if (status != 'error') {
                        $(selectelement + ' .center_body').html(response);
                    } else {
                        $(selectelement + ' .center_body').html(xhr.statusText);
                    }

                });
            }
        }
        else if (ddiv && ddiv != '') {
            $(selectelement + ' .center_body').empty();
            $(selectelement + ' .center_body').html($(ddiv).html());
        }
        if (xscroll) {
            $(selectelement + ' .center_body').css("overflow-x", 'scroll');
        }
        if (yscroll) {
            $(selectelement + ' .center_body').css("overflow-y", 'scroll');
        }
    }

    /**
     * 设置标题
     * @returns {} 
     */
    function setTitle() {
        $(selectelement + ' .title_value').html(dtitle);
    }

    /**
     * 创建弹出层控件
     * @param {} dom 
     * @returns {} 
     */
    function createdialog(dom) {
        if (dom == '') {
            $('body').html('<div class="Mydialog_' + dindex + '"> </div>');
            selectelement = '.Mydialog_' + dindex;
        }
        dhtml = gethtml();
        $(selectelement).html(dhtml);
        $(selectelement + ' .button_cancel').bind("click", function () {
            var pfather = $(this).attr('pfather');
            var obj = thisobject(pfather);
            var callback = obj.fnoptions.onCancelClick;
            if (callback && $.isFunction(callback)) {
                callback(mydialog, obj.name);
            }
        });
        $(selectelement + ' .button_ok').bind("click", function () {
            var pfather = $(this).attr('pfather');
            var obj = thisobject(pfather);
            var callback = obj.fnoptions.onOkClick;
            if (callback && $.isFunction(callback)) {
                callback(mydialog, obj.name);
            }
        });
        setSize();
        setTitle();
        setContent();
    }

    /**
     * 设置Options的值
     * @returns {} 
     */
    function setOptions(dom) {
        dtitle = dom.dtitle;
        dindex = dom.dindex;
        dialogwidth = dom.width;
        dialogheight = dom.height;
        ddiv = dom.ddiv;
        durl = dom.durl;
        dialogleft = dom.left;
        dialogtop = dom.top;
        dcenter = dom.dcenter;
        onOkClick = dom.onOkClick;
        onCancelClick = dom.onCancelClick;
        lbuttonvalue = dom.lbuttonvalue;
        rbuttonvalue = dom.rbuttonvalue;
        startIframe = dom.startIframe;
        lbuttonShow = dom.lbuttonShow;
        rbuttonShow = dom.rbuttonShow;
    }

    /**
     * 获取html拼接字符串输出到HTML元素用
     * @returns {} 
     */ /**
     * 属性
     */
    zDiaLog.prototype = {
        Init: function () {
            setOptions(this.options);
            var isexit = false;
            if (indexselectarray.length == 0) {
                indexselectarray.push({ name: dindex, value: selectelement, fnoptions: _options});
            } else {
                for (var i = 0; i < indexselectarray.length; i++) {
                    var obj = indexselectarray[i];
                    if (obj.name == dindex) {
                        isexit = true;
                        break;
                    }
                }
                if (!isexit) {
                    indexselectarray.push({ name: dindex, value: selectelement , fnoptions: _options});
                }
            }
            createdialog(selectelement);
        }
    }


    /**
	 * 初始化自定义模态框
	 * @param {} options 
	 * @returns {selector} 
	 */
    $.fn.myShowDiaLog = function (options) {
        _options = options;
        //创建zDiaLog的实体
        var zdialog = new zDiaLog(this, _options);
        //调用其方法
        zdialog.Init();
        /**
         * 显示对话框
         * @returns {NULL} 
         */
        this.ShowDiaLog = function (dom) {
            var thiss = thisselect(dom);
            $('.showdialogbackgroud').css('z-index', 9999 + dom);
            $(thiss).css("display", "");
        }
        /**
         * 隐藏对话框
         * @returns {NULL} 
         */
        this.HiddenDiaLog = function (dom) {
            var thiss = thisselect(dom);
            $('.showdialogbackgroud').css('z-index', 9999 + dom - 1);
            if (dom == 0) {
                $('.showdialogbackgroud').css('z-index', 0);
            }
            $(thiss).css("display", "none");
        }

        /**
         * 重新设置参数
         * @param {} data 
         * @returns {} 
         */
        this.ChangeShowDiaLog = function (data) {



            debugger;
        }
        mydialog = this;
        return this;

    };
})(jQuery);
//$.fn.myShowDiaLog.$ = $;
//})(jQuery, window, document);
