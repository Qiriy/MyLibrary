﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Webtest.webservice
{
    /// <summary>
    /// mywebservice 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class mywebservice : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public byte[] GetModel(byte[] b, string a)
        {
           // var dd = ETT.Library.ETTHelper.Factory.CCreate.CreateNewOjb(@"F:\我的文档\文档\Visual Studio 2013\Projects\ETTLibrary\TESTConsole\bin\Debug\MyModel.dll", "MyModel.Class1");
            var mm = ETT.Library.ETTHelper.EntityConvert.DataChange.RetrieveObject(b);
            //var cc = (MyModel.Class1)dd;


            var t = ETT.Library.ETTHelper.EntityConvert.DataChange.GetBinaryFormatData(mm);
            return t;
        }
    }
}
