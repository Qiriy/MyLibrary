﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Webtest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Vcode()
        {
            var code = ETT.Library.ETTHelper.CImage.VerificationCode.GenerateCheckCode(3);
            var img = ETT.Library.ETTHelper.CImage.VerificationCode.CreateImageCode(code,60,25);
            return File(img, @"iamge/Png");
        }

        public string SubmitFile(HttpPostedFileBase filesm)
        {
            var files = filesm;
            return "";
        }

     
        public string filesearch()
        {
            WebRequest wrt = WebRequest.Create(@"http://61.181.128.237:8056/UpLoadFile/0a8b3f78-a2ba-460a-8a23-4ebcd84c744c/%201-1/项目终验分项集成单位技术确认意见.pdf");
            //获取对应HTTP请求的响应
            WebResponse wrse = wrt.GetResponse();
            wrse.Headers.Add("Access-Control-Allow-Origin", "*");
            //获取响应流
            Stream strM = wrse.GetResponseStream();
            //对接响应流(以"GBK"字符集)
            StreamReader sOut = new StreamReader(strM, Encoding.Default);
            var str = sOut.ReadToEnd();
            return str;
        }
    }
}