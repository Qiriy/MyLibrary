﻿using System.Collections.Generic;
using ETT.Library.LogManager.Dal;
using ETT.Library.LogManager.Models;

namespace ETT.Library.LogManager.Bll
{
    internal class LogBll
    {
        #region<<字段>>


        #endregion

        /// <summary>
        /// 日志列表对外查询接口
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <param name="index"></param>
        /// <param name="pagesize"></param>
        /// <param name="datacount"></param>
        /// <returns></returns>
        public List<LogManager.Models.ViewModel> GetList(LogManager.Models.SearchModel query, LogManager.Enum.LogType type, int index, int pagesize,out int datacount)
        {
            return LogDal.GetList(query, type, index, pagesize, out datacount);
        }

        /// <summary>
        /// 日志实体对外查询接口
        /// </summary>
        /// <param name="query"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public LogManager.Models.ViewModel GetModel(LogManager.Models.SearchModel query, LogManager.Enum.LogType type)
        {
            return LogDal.GetModel(query, type);
        }

        /// <summary>
        /// 添加一条日志记录
        /// </summary>
        /// <param name="mdl"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool Add(LogManager.Models.Log_Model mdl, LogManager.Enum.LogType type)
        {
            return LogDal.Add(mdl, type);
        }

    }
}
