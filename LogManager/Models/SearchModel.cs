﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.LogManager.Models
{
    /// <summary>
    /// 查询模型
    /// </summary>
    public class SearchModel
    {
        /// <summary>
        /// guid编号
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 日志正文内容
        /// </summary>
        public string LM_Content { get; set; }

        /// <summary>
        /// 发生位置
        /// </summary>
        public string LM_Site { get; set; }

        /// <summary>
        /// 日志记录开始时间
        /// </summary>
        public DateTime? LM_Date_Begin { get; set; }

        /// <summary>
        /// 日志记录结束时间
        /// </summary>
        public DateTime? LM_Date_End { get; set; }

        /// <summary>
        /// 操作方
        /// </summary>
        public string LM_Operator { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        public string LM_IP { get; set; }

        /// <summary>
        /// 项目GUID
        /// </summary>
        public Guid? LM_ProgectID { get; set; }

        /// <summary>
        /// 日志类型:0=错误日志,1=操作日志,2=系统日志,3=登录日志
        /// </summary>
        public short LM_Type { get; set; }
    }
}
