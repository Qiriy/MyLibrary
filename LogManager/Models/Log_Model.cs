﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.LogManager.Models
{
    /// <summary>
    /// 日志Model
    /// </summary>
    public class Log_Model
    {
        /// <summary>
        /// guid编号
        /// </summary>
        [Description("ID")]
        public Guid ID { get; set; }

        /// <summary>
        /// 日志正文内容
        /// </summary>
        [Description("LM_Content")]
        public string LM_Content { get; set; }

        /// <summary>
        /// 发生位置
        /// </summary>
        [Description("LM_Site")]
        public string LM_Site { get; set; }

        /// <summary>
        /// 日志记录时间
        /// </summary>
        [Description("LM_Date")]
        public DateTime? LM_Date { get; set; }

        /// <summary>
        /// 操作方
        /// </summary>
        [Description("LM_Operator")]
        public string LM_Operator { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        [Description("LM_IP")]
        public string LM_IP { get; set; }

        /// <summary>
        /// 项目GUID
        /// </summary>
        [Description("LM_ProgectID")]
        public Guid? LM_ProgectID { get; set; }

        /// <summary>
        /// 日志类型:0=错误日志,1=操作日志,2=系统日志,3=登录日志
        /// </summary>
        [Description("LM_Type")]
        public short LM_Type { get; set; }
    }
}
