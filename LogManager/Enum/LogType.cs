﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Library.LogManager.Enum
{
    /// <summary>
    /// 日志类型枚举
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// 错误日志
        /// </summary>
        [Description("Error_Log")]
        ErrorLog = 0,

        /// <summary>
        /// 操作日志
        /// </summary>
        [Description("Operate_Log")]
        OperatorLog = 1,

        /// <summary>
        /// 系统日志（自动或运维）
        /// </summary>
        [Description("System_Log")]
        SystemLog = 2,

        /// <summary>
        /// 登录日志
        /// </summary>
        [Description("System_Log")]
        LoginLog = 3

    }
}
