﻿using System;
using System.Collections.Generic;
using System.Linq;
using ETT.Library.LogManager.Bll;
using ETT.Library.LogManager.Enum;
using ETT.Library.LogManager.Models;

namespace ETT.Library.LogManager
{
    /// <summary>
    /// 日志管理对外提供借口类
    /// </summary>
    public class Interface
    {
        #region<<字段>>

        /// <summary>
        /// 查询接口BLL
        /// </summary>
        private   Bll.LogBll _bll = new LogBll();

        #endregion

        /// <summary>
        /// 日志列表对外查询接口
        /// </summary>
        /// <param name="query">查询实体</param>
        /// <param name="type"></param>
        /// <param name="index">索引从 1 开始</param>
        /// <param name="pagesize"></param>
        /// <param name="datacount"></param>
        /// <returns></returns>
        public List<ViewModel> GetList(SearchModel query, LogManager.Enum.LogType type, int index, int pagesize, out int datacount)
        {
            return _bll.GetList(query, type, index, pagesize,out datacount);
        }

        /// <summary>
        /// 日志实体对外查询接口
        /// </summary>
        /// <param name="query">查询实体</param>
        /// <param name="type"></param>
        /// <returns></returns>
        public  ViewModel GetModel(SearchModel query, LogType type)
        {
            return _bll.GetModel(query, type);
        }

        /// <summary>
        /// 添加一条日志记录
        /// </summary>
        /// <param name="mdl">添加用实体</param>
        /// <param name="type">添加数据类型</param>
        /// <returns></returns>
        public  bool Add(Log_Model mdl, LogType type)
        {
            return _bll.Add(mdl, type);
        }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectID
        {
            get { return _projectid; }
        }

        private string _projectid = System.Configuration.ConfigurationManager.AppSettings["ProjectID"];
    }
}
