﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ETT.Library.DBHelper;
using ETT.Library.ETTHelper.EntityConvert;
using ETT.Library.ETTHelper.TypeHandle;
using ETT.Library.LogManager.Enum;
using ETT.Library.LogManager.Models;

namespace ETT.Library.LogManager.Dal
{
    /// <summary>
    /// 日志管理数据交互层
    /// </summary>
    public class LogDal
    {
        #region<<数据库连接字段>>


        /// <summary>
        /// 数据库连接字符串的配置节名称
        /// </summary>
        private static readonly string DBAPPSETTINGCONNECTIONSTRING =
            System.Configuration.ConfigurationManager.AppSettings["DBString"];

        /// <summary>
        /// 数据库链接字符串
        /// </summary>
        private static string CONNECTIONSTRING = System.Configuration.ConfigurationManager.AppSettings[DBAPPSETTINGCONNECTIONSTRING];


        #endregion

        internal static bool Add(Models.Log_Model mdl, Enum.LogType type)
        {
            bool bresult = true;
            try
            {
                ConnectionString.CONNECTIONSTRING = "";
                StringBuilder strsql = new StringBuilder();
                strsql.AppendFormat("INSERT INTO {0} ", EntityHelper<ViewModel>.GetEnumDes(type));
                strsql.AppendFormat(@" ( {0}[LM_Content],[LM_Site],[LM_Date],[LM_Operator],[LM_IP],[LM_ProgectID],[LM_Type] )", mdl.ID != Guid.Empty ? "ID," : "");
                strsql.Append("VALUES (");
                strsql.AppendFormat(@" {0}@LM_Content,@LM_Site,@LM_Date,@LM_Operator,@LM_IP,@LM_ProgectID,@LM_Type ", mdl.ID != Guid.Empty ? "@ID," : "");
                strsql.Append(")");
                var parms = GetSqlParameters<Models.Log_Model>(mdl);
                var i = MSSQLHelper.ExecuteSql(strsql.ToString(), CONNECTIONSTRING, parms);
                if (i <= 0)
                {
                    bresult = false;
                }
            }
            catch (Exception e)
            {
                bresult = false;
                ETT.Library.ETTHelper.LogHelper.WriteError.WriteErrorLog(e.Message);
            }
            return bresult;
        }


        /// <summary>
        /// 获取查询列表 从一开始索引
        /// </summary>
        /// <param name="qModel"></param>
        /// <param name="type"></param>
        /// <param name="index"></param>
        /// <param name="pagesize"></param>
        /// <param name="datacount">[输出]数据条数</param>
        /// <returns></returns>
        internal static List<ViewModel> GetList(SearchModel qModel, Enum.LogType type, int index, int pagesize, out int datacount)
        {
            datacount = 0;
            List<ViewModel> lst = null;
            StringBuilder strsql = new StringBuilder();
            strsql.AppendFormat(
                "SELECT [ID],[LM_Content],[LM_Site],[LM_Date],[LM_Operator],[LM_IP],[LM_ProgectID],[LM_ProgectName],[LM_Type] " +
                " FROM (SELECT ROW_NUMBER() OVER (ORDER BY A.ID) AS ROWID,A.[ID],[LM_Content],[LM_Site],[LM_Date],[LM_Operator],[LM_IP],[LM_ProgectID],B.[PT_Name] AS LM_ProgectName,[LM_Type] FROM {0} A LEFT JOIN ProjectTable B ON A.LM_ProgectID = B.ID  WHERE 1=1",
                EntityHelper<ViewModel>.GetEnumDes(type));
            StringBuilder strwhere = new StringBuilder();
            if (qModel != null)
            {
                if (!qModel.ID.IsEmpty())
                {
                    strwhere.AppendFormat(" AND A.[ID] ={0} ", qModel.ID);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_Content))
                {
                    strwhere.AppendFormat(" AND [LM_Content] LIKE '%{0}%' ", qModel.LM_Content);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_IP))
                {
                    strwhere.AppendFormat(" AND [LM_IP] LIKE '%{0}%' ", qModel.LM_IP);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_Operator))
                {
                    strwhere.AppendFormat(" AND [LM_Operator] LIKE '%{0}%' ", qModel.LM_Operator);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_Site))//TODO:记录日志的模块，功能或者方法
                {
                    strwhere.AppendFormat(" AND [LM_Site] LIKE '%{0}%' ", qModel.LM_Site);
                }
                if (qModel.LM_Date_Begin != null)
                {
                    strwhere.AppendFormat(" AND [LM_Date] >= '%{0}%' ", qModel.LM_Date_Begin.Value.ToString("yyyy-MM-dd") + " 00:00:00");
                }
                if (qModel.LM_Date_End != null)
                {
                    strwhere.AppendFormat(" AND [LM_Date] <= '%{0}%' ", qModel.LM_Date_End.Value.ToString("yyyy-MM-dd") + " 23:59:59");
                }
                if (qModel.LM_ProgectID != null && !qModel.LM_ProgectID.Value.IsEmpty())
                {
                    strwhere.AppendFormat(" AND [LM_ProgectID] = '{0}' ", qModel.LM_ProgectID.Value);
                }
                if ((int)type == 2 || (int)type == 3)
                {
                    strwhere.AppendFormat(" AND [LM_Type] = '{0}' ", (int)type);
                }
            }
            var begin = ((index - 1) * pagesize) + 1;
            var end = index * pagesize;
            strsql.Append(strwhere);
            strsql.AppendFormat(" ) M WHERE ROWID BETWEEN {0} AND {1}", begin, end);
            strsql.AppendFormat("; SELECT COUNT(1) FROM {0} A LEFT JOIN ProjectTable B ON A.LM_ProgectID = B.ID WHERE 1=1 {1}", EntityHelper<ViewModel>.GetEnumDes(type),strwhere);
            var ds = MSSQLHelper.Query(strsql.ToString(), CONNECTIONSTRING);
            if (ds != null && ds.Tables.Count > 1)
            {
                lst =
                   ETT.Library.ETTHelper.EntityConvert.EntityHelper<ViewModel>.ToList<ViewModel>(ds.Tables[0]);
                datacount = (int)ds.Tables[1].Rows[0][0];
            }
            if (lst == null)
            {
                lst = new List<ViewModel>();
            }
            return lst;
        }

        /// <summary>
        /// 获取实体中所有属性的 描述
        /// </summary>
        /// <typeparam name="T">实体类</typeparam>
        /// <param name="t">实例化后的实体类</param>
        /// <returns></returns>
        internal static string[] GetDescription<T>(T t)
        {
            var props = t.GetType().GetProperties();
            return props.Select(item => ((DescriptionAttribute)Attribute.GetCustomAttribute(item, typeof(DescriptionAttribute))).Description).ToArray();
        }

        /// <summary>
        /// 获取实体中所有属性的 描述并生成sqlparameter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        internal static SqlParameter[] GetSqlParameters<T>(T t) where T : class,new()
        {
            List<SqlParameter> paras = new List<SqlParameter>();
            var props = t.GetType().GetProperties();
            foreach (var item in props)
            {
                var name =
                    ((DescriptionAttribute)Attribute.GetCustomAttribute(item, typeof(DescriptionAttribute)))
                        .Description;
                var val = item.GetValue(t, null);
                SqlParameter param = new SqlParameter(name, val != null ? val : DBNull.Value);
                paras.Add(param);
            }
            return paras.ToArray();
        }

        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="qModel"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ViewModel GetModel(SearchModel qModel, LogType type)
        {
            List<ViewModel> lst = null;
            StringBuilder strsql = new StringBuilder();
            strsql.AppendFormat(
                "SELECT TOP 1 A.[ID],[LM_Content],[LM_Site],[LM_Date],[LM_Operator],[LM_IP],[LM_ProgectID],B.[PT_Name] AS LM_ProgectName,[LM_Type] FROM {0} A LEFT JOIN ProjectTable B ON A.LM_ProgectID = B.ID  WHERE 1=1",
                EntityHelper<ViewModel>.GetEnumDes(type));
            if (qModel != null)
            {
                if (!qModel.ID.IsEmpty())
                {
                    strsql.AppendFormat(" AND A.[ID] ='{0}' ", qModel.ID);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_Content))
                {
                    strsql.AppendFormat(" AND [LM_Content] LIKE '%{0}%' ", qModel.LM_Content);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_IP))
                {
                    strsql.AppendFormat(" AND [LM_IP] LIKE '%{0}%' ", qModel.LM_IP);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_Operator))
                {
                    strsql.AppendFormat(" AND [LM_Operator] LIKE '%{0}%' ", qModel.LM_Operator);
                }
                if (!string.IsNullOrWhiteSpace(qModel.LM_Site))//TODO:记录日志的模块，功能或者方法
                {
                    strsql.AppendFormat(" AND [LM_Site] LIKE '%{0}%' ", qModel.LM_Site);
                }
                if (qModel.LM_Date_Begin != null)
                {
                    strsql.AppendFormat(" AND [LM_Date] >= '%{0}%' ", qModel.LM_Date_Begin.Value.ToString("yyyy-MM-dd") + " 00:00:00");
                }
                if (qModel.LM_Date_End != null)
                {
                    strsql.AppendFormat(" AND [LM_Date] <= '%{0}%' ", qModel.LM_Date_End.Value.ToString("yyyy-MM-dd") + " 23:59:59");
                }
                if (qModel.LM_ProgectID != null && !qModel.LM_ProgectID.Value.IsEmpty())
                {
                    strsql.AppendFormat(" AND [LM_ProgectID] = '{0}' ", qModel.LM_ProgectID.Value);
                }
                if ((int)type == 2 || (int)type == 3)
                {
                    strsql.AppendFormat(" AND [LM_Type] = '{0}' ", (int)type);
                }
            }
            var ds = MSSQLHelper.Query(strsql.ToString(), CONNECTIONSTRING);
            if (ds != null && ds.Tables.Count > 0)
            {
                lst =
                    ETT.Library.ETTHelper.EntityConvert.EntityHelper<ViewModel>.ToList<ViewModel>(ds.Tables[0]);
            }
            var mdl = new ViewModel();
            if (lst != null)
            {
                mdl = lst.SingleOrDefault();
            }
            return mdl;
        }
    }
}
