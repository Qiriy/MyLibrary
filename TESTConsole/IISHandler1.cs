﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace TESTConsole
{


    public class IISHandler1 : IHttpHandler
    {
        /// <summary>
        /// 您将需要在网站的 Web.config 文件中配置此处理程序 
        /// 并向 IIS 注册它，然后才能使用它。有关详细信息，
        /// 请参见下面的链接: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // 如果无法为其他请求重用托管处理程序，则返回 false。
            // 如果按请求保留某些状态信息，则通常这将为 false。
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //在此处写入您的处理程序实现。

            WebRequest wrt = WebRequest.Create(@"http://61.181.128.237:8056/UpLoadFile/0a8b3f78-a2ba-460a-8a23-4ebcd84c744c/%201-1/项目终验分项集成单位技术确认意见.pdf");
            //获取对应HTTP请求的响应
            WebResponse wrse = wrt.GetResponse();
            //获取响应流
            Stream strM = wrse.GetResponseStream();
            //对接响应流(以"GBK"字符集)
            StreamReader SR = new StreamReader(strM, Encoding.GetEncoding("UTF-8"));
            string strallstrm = SR.ReadToEnd();
            context.Response.Write(strallstrm);

        }

        #endregion
    }
}
