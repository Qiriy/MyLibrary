﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ETT.Library.ETTHelper.TypeHandle;

namespace TESTConsole
{
    public class AL
    {
        public static void AddValue()
        {
            List<Modelinput> list;
            StringBuilder str = new StringBuilder();
            str.Append("SELECT * FROM test");
            ETT.Library.DBHelper.ConnectionString.CONNECTIONSTRING = @"server=192.168.0.24\sql2008;database=PerformanceDB;uid=sa;pwd=12345678";
            var ds = ETT.Library.DBHelper.MSSQLHelper.Query(str.ToString());
            if (ds != null && ds.Tables.Count > 0)
            {
                list = ETT.Library.ETTHelper.EntityConvert.EntityHelper<Modelinput>.ToList<Modelinput>(ds.Tables[0]);
                if (list == null)
                {
                    list = new List<Modelinput>();
                }
                foreach (var item in list)
                {
                    string strsql = @"insert MiddleCalculateReport ( KPI_ID,MCR_ProcdureName,MCR_Year, DP_ID, MCR_Month, MCR_Value) 
                                values ('491383E0-B32E-495F-BCD6-EA0DD7301AC5','Procedure_y','2016','" + item.PID.ToString() + "','08'," + item.y + ")";
                    ETT.Library.DBHelper.MSSQLHelper.ExecuteSql(strsql);
                    strsql = @"insert MiddleCalculateReport ( KPI_ID,MCR_ProcdureName,MCR_Year, DP_ID, MCR_Month, MCR_Value) 
                                values ('76CFAC72-C647-4C0D-84C0-C6876194D7AB','Procedure_ss','2016','" + item.PID.ToString() + "','08'," + item.ss + ")";
                    ETT.Library.DBHelper.MSSQLHelper.ExecuteSql(strsql);
                    strsql = @"insert MiddleCalculateReport ( KPI_ID,MCR_ProcdureName,MCR_Year, DP_ID, MCR_Month, MCR_Value) 
                                values ('5AC8AAE0-E84E-4E26-BF00-DDCCE9CE7C5D','Procedure_cz','2016','" + item.PID.ToString() + "','08'," + item.cz + ")";
                    ETT.Library.DBHelper.MSSQLHelper.ExecuteSql(strsql);
                    strsql = @"insert MiddleCalculateReport ( KPI_ID,MCR_ProcdureName,MCR_Year, DP_ID, MCR_Month, MCR_Value) 
                                values ('1E3875AD-0CD9-4738-8CF6-B198D03CFA83','Procedure_hz','2016','" + item.PID.ToString() + "','08'," + item.hz + ")";
                    ETT.Library.DBHelper.MSSQLHelper.ExecuteSql(strsql);
                    strsql = @"insert MiddleCalculateReport ( KPI_ID,MCR_ProcdureName,MCR_Year, DP_ID, MCR_Month, MCR_Value) 
                                values ('85F8887E-D7EA-41A2-9BF3-B68E3758ADBA','Procedure_yb','2016','" + item.PID.ToString() + "','08'," + item.yb + ")";
                    ETT.Library.DBHelper.MSSQLHelper.ExecuteSql(strsql);
                    strsql = @"insert MiddleCalculateReport ( KPI_ID,MCR_ProcdureName,MCR_Year, DP_ID, MCR_Month, MCR_Value) 
                                values ('297412ED-1676-4042-A699-3D64ED30D639','Procedure_zy','2016','" + item.PID.ToString() + "','08'," + item.zy + ")";
                    ETT.Library.DBHelper.MSSQLHelper.ExecuteSql(strsql);
                    strsql = @"insert MiddleCalculateReport ( KPI_ID,MCR_ProcdureName,MCR_Year, DP_ID, MCR_Month, MCR_Value) 
                                values ('C343CAEF-FEB2-4317-9F59-79EE254E2ABD','Procedure_zzjx','2016','" + item.PID.ToString() + "','08'," + item.ZZ + ")";
                    ETT.Library.DBHelper.MSSQLHelper.ExecuteSql(strsql);
                }
            }
        }


    }

    public class Modelinput
    {
        public int ID { get; set; }

        public Guid PID { get; set; }

        public decimal y { get; set; }

        public decimal ss { get; set; }

        public decimal hz { get; set; }

        public decimal yb { get; set; }

        public decimal zy { get; set; }

        public decimal cz { get; set; }

        public decimal ZZ { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class TaskModule 
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public TaskModule()
        {
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="m"></param>
        public TaskModule(TaskMedthod m)
        {
            Medthod = m;
        }



        public  void Init()
        {
            m = new TaskModule();
            td = new Thread(new ThreadStart(Medthod));
            td.Name = _tdname;
            td.Start();

        }

        /// <summary>
        /// TaskMoulde属性
        /// </summary>
        public TaskModule m { get; set; }

        /// <summary>
        /// td线程
        /// </summary>
        public Thread td { get; set; }

        /// <summary>
        /// 方法
        /// </summary>
        public TaskMedthod Medthod { get; set; }

        private string _tdname;

        /// <summary>
        /// 线程名称
        /// </summary>
        public string tdname
        {
            set
            {
                if (string.IsNullOrWhiteSpace(value) || string.IsNullOrWhiteSpace(_tdname))
                {
                    _tdname = DateTime.Now.ToString("ffff");
                }
                else
                {
                    _tdname = value + DateTime.Now.ToString("ffff");
                }
            }
        }

        /// <summary>
        /// 委托
        /// </summary>
        public delegate void TaskMedthod();
    }
}
